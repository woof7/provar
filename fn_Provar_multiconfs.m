%% Provar - CONCOORD or tCONCOORD
%  For any set of identical sequence structure conformations indexed
%  * Note: no sequence alignments attempted in this module!

function params = fn_Provar_multiconfs(params)
    
    status='';
  
    %no_structs=0;
    for iProgIdx=1:1:params.pocketProgNo
        no_structs(iProgIdx)=0;
    end
    
   not_matched=1;   % For rrror trapping 

   params.status.fn_Provar_multiconfs='OK';
    % ** PROCESS STRUCTURE-PREDICTION PAIRS
    %structure_files=dir(strcat(params.base_dir,params.structure_dir));
    structure_files=dir(params.structure_dir);
    
    %if size(structure_files,1)==0, disp('*No structures found*'), end;

    for iStructFileLoop=1:1:size(structure_files,1)
        if ~(   isempty(regexp(structure_files(iStructFileLoop).name,'_fs[0-9]*.pdb', 'once' )) && ...
                isempty(regexp(structure_files(iStructFileLoop).name,'_H[0-9]*.pdb', 'once' )) && ...
                isempty(regexp(structure_files(iStructFileLoop).name,'tdisco[0-9]*.pdb', 'once' )) && ...
                isempty(regexp(structure_files(iStructFileLoop).name,'NMR_0.[0-9]*.pdb', 'once' )) && ...
                isempty(regexp(structure_files(iStructFileLoop).name,'_R[0-9]*.pdb', 'once' )) && ...
                isempty(regexp(structure_files(iStructFileLoop).name,'[0-9]*.pdb', 'once' )))

            [conformer_no,params]=fn_Provar_getFileSequence(params,status,structure_files(iStructFileLoop).name);
            
            if (conformer_no>-1 && strcmp(params.status.fn_Provar_getFileSequence,'OK'))
                struct_file=fullfile(params.structure_dir,structure_files(iStructFileLoop).name);

                % Load structure
                disp(strcat('Reading structure :',struct_file));
                [structure,conf,params]=fn_Provar_loadStructure(params,struct_file);
                if ~strcmp(params.status.fn_Provar_loadStructure,'OK')
                    % Some problem...
                    disp(params.status.fn_Provar_loadStructure);
                    disp('');

                else
                    % Load pocket prediction (for all defined progs as ref'd in
                    % setParams).  Get struct array back in pocket_single - one for
                    % PASS, LIGSITE etc...
                    [pocket_single,params]=fn_Provar_loadPocketPredictionSingle(params,struct_file,conformer_no);

                    if strcmp(params.status.fn_Provar_loadPocketPredictionSingle,'OK')
                        %no_structs=no_structs+1;

                        disp('Testing pocket-lining residues and atoms...');

                        for iProgIdx=1:1:params.pocketProgNo

                            binary_amino{iProgIdx,conformer_no}=0;
                            binary_atom{iProgIdx,conformer_no}=0;
                            res_tally{iProgIdx,conformer_no}=0;

                            % Only if we have pocket data for this prog!
                            if ~isempty(pocket_single{iProgIdx})

                                % Check status for this prog - there may have been
                                % no pockets predicted or a missing file
                                if pocket_single{iProgIdx}.status==0

                                    not_matched=0;
                                    %no_structs=no_structs+1;
                                    no_structs(iProgIdx)=no_structs(iProgIdx)+1;

                                    % Get pocket-lining...
                                    [binary_amino{iProgIdx,conformer_no}, binary_atom{iProgIdx,conformer_no}, res_tally{iProgIdx,conformer_no},res_avg{iProgIdx,conformer_no}] =  ...
                                            fn_getPocketLining(params, structure, 1,pocket_single{iProgIdx}, params.pocketDirect{iProgIdx});

                                    % Create totals
                                    if no_structs(iProgIdx)==1
                                        provar_amino_total{iProgIdx}=binary_amino{iProgIdx,conformer_no};
                                        provar_atom_total{iProgIdx}= binary_atom{iProgIdx,conformer_no};
                                        provar_amino_avg{iProgIdx}=res_avg{iProgIdx,conformer_no};
                                    else
                                        provar_amino_total{iProgIdx}=provar_amino_total{iProgIdx} + binary_amino{iProgIdx,conformer_no};
                                        provar_atom_total{iProgIdx}= provar_atom_total{iProgIdx} +binary_atom{iProgIdx,conformer_no};

                                        provar_amino_avg{iProgIdx}=provar_amino_avg{iProgIdx} + res_avg{iProgIdx,conformer_no};
                                    end

                                end

                            end
                        end
                    else
                        % pass on sub-fns warning...
                        disp(params.status.fn_Provar_loadPocketPredictionSingle);
                    end
                end
                    % Warning passed through...
                    disp(params.status.fn_Provar_getFileSequence);
            end
            disp('==================================');
        end
    end

    if not_matched==1
           params.status.fn_Provar_multiconfs='*Error generating Provar totals - probabilities not written... Check correct correspondence of file and pocket file directory structure *';
      
    else
       
        % Generate probability values and quartile statistics
        for iProgIdx=1:1:params.pocketProgNo
            
           if (params.progrun{iProgIdx}==1) && (~isempty(pocket_single{iProgIdx}))
            
                % Probabilities
                provar_amino_p{iProgIdx}=provar_amino_total{iProgIdx} / no_structs(iProgIdx);
                provar_atom_p{iProgIdx}= provar_atom_total{iProgIdx} / no_structs(iProgIdx);
                provar_amino_avg_p{iProgIdx}=provar_amino_avg{iProgIdx} / no_structs(iProgIdx);

                % Quantiles
                % *********
                params.stats{iProgIdx}.amino=num2str(sprintf('% 1.3f',quantile(provar_amino_p{iProgIdx},[.25,0.50,0.75])));
                params.stats{iProgIdx}.amino_avg=num2str(sprintf('% 1.3f',quantile(provar_amino_avg_p{iProgIdx},[.25,0.50,0.75])));
                params.stats{iProgIdx}.atom=num2str(sprintf('% 1.3f',quantile(provar_atom_p{iProgIdx},[.25,0.50,0.75])));
                % Normalised atom removes (what can be dominant) zero probability atoms   
                provar_atom_p_norm{iProgIdx}=provar_atom_p{iProgIdx};
                provar_atom_p_norm{iProgIdx}(provar_atom_p{iProgIdx}==0)=[];
                params.stats{iProgIdx}.atom_norm=num2str(sprintf('% 1.3f',quantile(provar_atom_p_norm{iProgIdx},[.25,0.50,0.75])));

                disp('');
                disp(strcat('Quantiles         ( 0.25,0.50,0.75) for :',params.pocketProg{iProgIdx}));
                disp(strcat('atom              : ',params.stats{iProgIdx}.atom));
                disp(strcat('atom normalised   : ',params.stats{iProgIdx}.atom_norm));    
                disp(strcat('amino             : ',params.stats{iProgIdx}.amino));
                disp(strcat('amino avg         : ',params.stats{iProgIdx}.amino_avg));

                params.p.provar_amino_p{iProgIdx}=provar_amino_p{iProgIdx};
                params.p.provar_atom_p{iProgIdx}=provar_atom_p{iProgIdx};
                params.p.provar_amino_avg_p{iProgIdx}=provar_amino_avg_p{iProgIdx};
                
                disp('----------------------------------------------------------');

                % -------------------------------------
                % Write output probability files ...
                disp('Writing probability files....');
                params=fn_Provar_write_pb_files(params,provar_amino_p,'p',iProgIdx,0);
                params=fn_Provar_write_pb_files(params,provar_atom_p,'p',iProgIdx,1);
                params=fn_Provar_write_pb_files(params,provar_amino_avg_p,'p',iProgIdx,2);
                disp(params.status.fn_Provar_write_pb_files);
                disp('');
                
                % Write PDB output files (update the B-factor with probability)
                disp('Writing PDB files....');
                params=fn_Provar_write_pdb_files(params, iProgIdx);
                disp(params.status.fn_Provar_write_pdb_files);
                disp('');
            
           end 
        end
    end
end


