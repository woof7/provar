% **************************************
% fn_Provar_params
% PROVAR PARAMETER FILE
% **************************************

% Set up user-parameters for Provar runs:
% Directory structures
% Which pocket programs
% Run IDs
% Alignment files (if nec)


function params=fn_Provar_params(params)
     
        
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  EXAMPLE 2 PARAMETERS FOR SET OF HOMOLOGUES                                              %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    % Example of a set of homologues, with the FASTA alignment file
    % supplied
    % IL-2 homologues apo only onto Receptor complex 2B5IA
    % ******************************************************************
    % P00127:G00103/P00137/LC00128/FD00012
    
    % This is the provar run ID - A directory of this name gets created
    % under base_dir for all outputs.  It is also used to name PDB and log
    % files.
    params.pdfid='example2_output';
    
    % Specify whether TC-like or homologues
    params.run_type='HOM';
  
    % BASE DIRECTORY
    % This is the root directory for all data and Provar outputs
     params.base_dir=fullfile(pwd,'example2_homologues');
    
    % Where's the reference structure?  Atom/amino-acid numbers use this
    % file.  Here we're using chaing A of the receptor complex structure.
    params.ref_pdb=fullfile(params.base_dir,'2B5IA.pdb');
    
    % Where are the PDB homologous structures to be found?
    params.structure_dir=fullfile(params.base_dir,'structures');

    % Essential to have valid FASTA alignment for homologues
    params.seq_align_file=fullfile(params.base_dir,'Mustang_alignment_apo_holo_receptor.fasta');
    
    %params.seq_align_file=fullfile(params.base_dir,'test1.afasta');
    params.useFASTA=1;      % Set to 1 for homol
    
    % Our alignment includes a holo structure (1M48B) and a receptor
    % structure (2B5IA) as we are analysing the 17 apo structures w.r.t.
    % these.  However, we *don't* want them included in Provar calculations
    % We set exclusions here - i.e. PDBs that are in the FASTA but should
    % be ignored for calculations.  Entries here are ones to ignore from the
    % fasta alignment, so ensure you put in structures even if they're not
    % actually in params.structure_dir.
    params.homSkip{1}=('2B5IA');
    params.homSkip{2}=('1M48B');

    % What is the pocket-prediction programs' root output dir?
    % (Optional - default is base_dir)
    params.run_dir=params.base_dir;

    % Note: Dual run - two pocket progs together
    % The directory for the PASS files
    params.pass_dir=fullfile(params.run_dir,'PASS_pocket_files');
    % The directory for the Ligsite files
    params.ligsite_dir=fullfile(params.run_dir,'Ligsite_pocket_files');
    % fPocket files
    params.fpocket_dir=fullfile(params.run_dir,'fPocket_pocket_files');
   
    % This will append to the PDB HEADER in updated files
    % of form "Provar v4.81/......"
    % Keep to <30 chars or will truncate.
    params.pdb_ref='IL-2 17 apo homologues onto 2B5I (receptor struct)';
    
    % Used for grouping pocket prog predictions that all related to same
    % system.  Any reasonable non-null value such as 'test' will do.
    params.groupid='example2_group';
    % Identifies data source - any reasonable non-null value is fine.
    params.datasetid='example2_data';

    params.cc_id='';
    
    % ----------------------------------------------  

  
 
% ****************************************************************
% FINAL CONFIGURATION - AUTOMATIC DO NOT CHANGE
% ****************************************************************
  params.app_dir=pwd;

  % The descriptive names for pocket progs aren't
  % used within code as loop-based... translate here.
  if ~strcmp(params.pass_dir,'')
     params.progid{1}= params.pass_dir;
  end
  
  if ~strcmp(params.ligsite_dir,'')
     params.progid{2}= params.ligsite_dir;
  end
  
  if ~strcmp(params.sitemap_dir,'')
     params.progid{3}= params.sitemap_dir;
  end
  
  if ~strcmp(params.fpocket_vert_dir,'')
     params.progid{4}= params.fpocket_vert_dir;
  end
  
  % Direct fpocket (where they've given pocket-lining atoms)
  if ~strcmp(params.fpocket_dir,'')
     params.progid{5}= params.fpocket_dir;
  end
  
  if ~strcmp(params.fpocket_atom_dir,'')
     params.progid{6}= params.fpocket_atom_dir;
  end
  
  if ~strcmp(params.generic_pocket_dir,'')
     params.progid{7}= params.generic_pocket_dir;
  end
  
  if ~strcmp(params.generic_pocket_direct_dir,'')
     params.progid{8}= params.generic_pocket_direct_dir;
  end
 
  for iLoop=1:1:(params.pocketProgNo)
  
        % The run flags just simplify some of the coding...
      if ~strcmp(params.progid{iLoop},'')
        params.progrun{iLoop}=1;
      end
  end
  
  % For PDB HEADER updates (max 40 chars)
  params.pdb_ref=strcat('Provar v',params.provar_version,'/',params.pdb_ref);
  if length(params.pdb_ref)>40
     params.pdb_ref=params.pdb_ref(1:40); 
  end

  % Set the FASTA flag if we have an alignment file
  if ~strcmp(params.seq_align_file,'')
        params.useFASTA=1;
  end
  
       
end