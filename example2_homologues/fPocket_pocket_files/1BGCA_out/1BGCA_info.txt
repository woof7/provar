Pocket 1 :
	Score : 			34.201
	Druggability Score : 		0.787
	Number of Alpha Spheres : 	49
	Total SASA : 			110.218
	Polar SASA : 			51.045
	Apolar SASA : 			59.174
	Volume : 			203.265
	Mean local hydrophobic density : 22.000
	Mean alpha sphere radius :	3.403
	Mean alp. sph. solvent access :  0.439
	Apolar alpha sphere proportion : 0.469
	Hydrophobicity score:		53.800
	Volume score: 			 4.000
	Polarity score:			 2
	Charge score :			 -1
	Proportion of polar atoms: 	35.714
	Alpha sphere density : 		3.178
	Cent. of mass - Alpha Sphere max dist: 7.305
	Flexibility : 			 0.012

Pocket 2 :
	Score : 			33.572
	Druggability Score : 		0.855
	Number of Alpha Spheres : 	46
	Total SASA : 			211.245
	Polar SASA : 			91.472
	Apolar SASA : 			119.773
	Volume : 			495.728
	Mean local hydrophobic density : 22.963
	Mean alpha sphere radius :	3.737
	Mean alp. sph. solvent access :  0.532
	Apolar alpha sphere proportion : 0.587
	Hydrophobicity score:		48.250
	Volume score: 			 5.750
	Polarity score:			 4
	Charge score :			 1
	Proportion of polar atoms: 	28.571
	Alpha sphere density : 		3.714
	Cent. of mass - Alpha Sphere max dist: 8.773
	Flexibility : 			 0.244

Pocket 3 :
	Score : 			30.597
	Druggability Score : 		0.555
	Number of Alpha Spheres : 	43
	Total SASA : 			156.723
	Polar SASA : 			72.031
	Apolar SASA : 			84.692
	Volume : 			430.721
	Mean local hydrophobic density : 21.000
	Mean alpha sphere radius :	3.769
	Mean alp. sph. solvent access :  0.574
	Apolar alpha sphere proportion : 0.512
	Hydrophobicity score:		34.000
	Volume score: 			 4.400
	Polarity score:			 4
	Charge score :			 3
	Proportion of polar atoms: 	35.714
	Alpha sphere density : 		3.916
	Cent. of mass - Alpha Sphere max dist: 7.662
	Flexibility : 			 0.113

Pocket 4 :
	Score : 			30.487
	Druggability Score : 		0.041
	Number of Alpha Spheres : 	46
	Total SASA : 			221.383
	Polar SASA : 			125.980
	Apolar SASA : 			95.403
	Volume : 			527.960
	Mean local hydrophobic density : 9.000
	Mean alpha sphere radius :	3.859
	Mean alp. sph. solvent access :  0.544
	Apolar alpha sphere proportion : 0.217
	Hydrophobicity score:		3.444
	Volume score: 			 3.667
	Polarity score:			 6
	Charge score :			 2
	Proportion of polar atoms: 	38.462
	Alpha sphere density : 		3.611
	Cent. of mass - Alpha Sphere max dist: 8.405
	Flexibility : 			 0.161

Pocket 5 :
	Score : 			24.456
	Druggability Score : 		0.676
	Number of Alpha Spheres : 	40
	Total SASA : 			228.429
	Polar SASA : 			85.929
	Apolar SASA : 			142.500
	Volume : 			411.155
	Mean local hydrophobic density : 18.000
	Mean alpha sphere radius :	3.865
	Mean alp. sph. solvent access :  0.552
	Apolar alpha sphere proportion : 0.475
	Hydrophobicity score:		68.333
	Volume score: 			 4.333
	Polarity score:			 0
	Charge score :			 0
	Proportion of polar atoms: 	38.462
	Alpha sphere density : 		3.066
	Cent. of mass - Alpha Sphere max dist: 8.411
	Flexibility : 			 0.340

