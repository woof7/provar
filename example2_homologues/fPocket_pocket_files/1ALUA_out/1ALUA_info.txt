Pocket 1 :
	Score : 			31.894
	Druggability Score : 		0.599
	Number of Alpha Spheres : 	85
	Total SASA : 			410.737
	Polar SASA : 			170.419
	Apolar SASA : 			240.318
	Volume : 			904.856
	Mean local hydrophobic density : 29.684
	Mean alpha sphere radius :	3.940
	Mean alp. sph. solvent access :  0.552
	Apolar alpha sphere proportion : 0.447
	Hydrophobicity score:		33.846
	Volume score: 			 3.923
	Polarity score:			 5
	Charge score :			 1
	Proportion of polar atoms: 	38.298
	Alpha sphere density : 		6.064
	Cent. of mass - Alpha Sphere max dist: 16.697
	Flexibility : 			 0.319

Pocket 2 :
	Score : 			30.578
	Druggability Score : 		0.500
	Number of Alpha Spheres : 	60
	Total SASA : 			158.637
	Polar SASA : 			94.633
	Apolar SASA : 			64.004
	Volume : 			223.384
	Mean local hydrophobic density : 30.765
	Mean alpha sphere radius :	3.371
	Mean alp. sph. solvent access :  0.388
	Apolar alpha sphere proportion : 0.567
	Hydrophobicity score:		-9.917
	Volume score: 			 3.750
	Polarity score:			 8
	Charge score :			 1
	Proportion of polar atoms: 	41.667
	Alpha sphere density : 		4.066
	Cent. of mass - Alpha Sphere max dist: 10.262
	Flexibility : 			 0.300

Pocket 3 :
	Score : 			24.264
	Druggability Score : 		0.610
	Number of Alpha Spheres : 	66
	Total SASA : 			316.197
	Polar SASA : 			130.222
	Apolar SASA : 			185.975
	Volume : 			668.085
	Mean local hydrophobic density : 27.524
	Mean alpha sphere radius :	3.797
	Mean alp. sph. solvent access :  0.496
	Apolar alpha sphere proportion : 0.636
	Hydrophobicity score:		40.250
	Volume score: 			 3.583
	Polarity score:			 5
	Charge score :			 1
	Proportion of polar atoms: 	39.474
	Alpha sphere density : 		5.725
	Cent. of mass - Alpha Sphere max dist: 12.314
	Flexibility : 			 0.650

Pocket 4 :
	Score : 			24.070
	Druggability Score : 		0.463
	Number of Alpha Spheres : 	56
	Total SASA : 			194.469
	Polar SASA : 			60.422
	Apolar SASA : 			134.047
	Volume : 			405.161
	Mean local hydrophobic density : 25.852
	Mean alpha sphere radius :	3.827
	Mean alp. sph. solvent access :  0.541
	Apolar alpha sphere proportion : 0.482
	Hydrophobicity score:		41.100
	Volume score: 			 3.700
	Polarity score:			 4
	Charge score :			 0
	Proportion of polar atoms: 	34.375
	Alpha sphere density : 		3.538
	Cent. of mass - Alpha Sphere max dist: 8.103
	Flexibility : 			 0.097

Pocket 5 :
	Score : 			23.192
	Druggability Score : 		0.050
	Number of Alpha Spheres : 	70
	Total SASA : 			316.615
	Polar SASA : 			182.569
	Apolar SASA : 			134.047
	Volume : 			868.469
	Mean local hydrophobic density : 17.000
	Mean alpha sphere radius :	3.822
	Mean alp. sph. solvent access :  0.534
	Apolar alpha sphere proportion : 0.400
	Hydrophobicity score:		23.000
	Volume score: 			 3.417
	Polarity score:			 5
	Charge score :			 1
	Proportion of polar atoms: 	39.024
	Alpha sphere density : 		5.455
	Cent. of mass - Alpha Sphere max dist: 13.954
	Flexibility : 			 0.313

Pocket 6 :
	Score : 			20.396
	Druggability Score : 		0.037
	Number of Alpha Spheres : 	40
	Total SASA : 			135.638
	Polar SASA : 			68.011
	Apolar SASA : 			67.627
	Volume : 			208.651
	Mean local hydrophobic density : 19.000
	Mean alpha sphere radius :	3.866
	Mean alp. sph. solvent access :  0.517
	Apolar alpha sphere proportion : 0.500
	Hydrophobicity score:		-12.625
	Volume score: 			 3.125
	Polarity score:			 6
	Charge score :			 -1
	Proportion of polar atoms: 	37.500
	Alpha sphere density : 		2.399
	Cent. of mass - Alpha Sphere max dist: 4.583
	Flexibility : 			 0.474

Pocket 7 :
	Score : 			18.603
	Druggability Score : 		0.103
	Number of Alpha Spheres : 	59
	Total SASA : 			262.848
	Polar SASA : 			134.840
	Apolar SASA : 			128.009
	Volume : 			610.076
	Mean local hydrophobic density : 5.286
	Mean alpha sphere radius :	3.744
	Mean alp. sph. solvent access :  0.527
	Apolar alpha sphere proportion : 0.237
	Hydrophobicity score:		37.000
	Volume score: 			 4.667
	Polarity score:			 7
	Charge score :			 0
	Proportion of polar atoms: 	30.769
	Alpha sphere density : 		5.075
	Cent. of mass - Alpha Sphere max dist: 10.848
	Flexibility : 			 0.372

Pocket 8 :
	Score : 			16.837
	Druggability Score : 		0.087
	Number of Alpha Spheres : 	44
	Total SASA : 			240.474
	Polar SASA : 			95.559
	Apolar SASA : 			144.915
	Volume : 			535.703
	Mean local hydrophobic density : 11.000
	Mean alpha sphere radius :	4.101
	Mean alp. sph. solvent access :  0.572
	Apolar alpha sphere proportion : 0.273
	Hydrophobicity score:		36.636
	Volume score: 			 4.818
	Polarity score:			 6
	Charge score :			 1
	Proportion of polar atoms: 	42.857
	Alpha sphere density : 		3.778
	Cent. of mass - Alpha Sphere max dist: 9.945
	Flexibility : 			 0.424

Pocket 9 :
	Score : 			12.500
	Druggability Score : 		0.018
	Number of Alpha Spheres : 	41
	Total SASA : 			291.290
	Polar SASA : 			182.604
	Apolar SASA : 			108.687
	Volume : 			695.756
	Mean local hydrophobic density : 2.000
	Mean alpha sphere radius :	4.296
	Mean alp. sph. solvent access :  0.548
	Apolar alpha sphere proportion : 0.073
	Hydrophobicity score:		-8.111
	Volume score: 			 3.222
	Polarity score:			 7
	Charge score :			 -1
	Proportion of polar atoms: 	59.259
	Alpha sphere density : 		4.657
	Cent. of mass - Alpha Sphere max dist: 9.825
	Flexibility : 			 0.586

Pocket 10 :
	Score : 			11.028
	Druggability Score : 		0.031
	Number of Alpha Spheres : 	38
	Total SASA : 			196.315
	Polar SASA : 			150.425
	Apolar SASA : 			45.890
	Volume : 			427.291
	Mean local hydrophobic density : 11.000
	Mean alpha sphere radius :	3.781
	Mean alp. sph. solvent access :  0.579
	Apolar alpha sphere proportion : 0.316
	Hydrophobicity score:		36.000
	Volume score: 			 4.857
	Polarity score:			 2
	Charge score :			 0
	Proportion of polar atoms: 	56.522
	Alpha sphere density : 		3.224
	Cent. of mass - Alpha Sphere max dist: 6.958
	Flexibility : 			 0.486

