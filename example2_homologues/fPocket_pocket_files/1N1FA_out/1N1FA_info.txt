Pocket 1 :
	Score : 			33.650
	Druggability Score : 		0.155
	Number of Alpha Spheres : 	75
	Total SASA : 			365.325
	Polar SASA : 			140.706
	Apolar SASA : 			224.619
	Volume : 			943.283
	Mean local hydrophobic density : 30.457
	Mean alpha sphere radius :	4.233
	Mean alp. sph. solvent access :  0.636
	Apolar alpha sphere proportion : 0.467
	Hydrophobicity score:		3.273
	Volume score: 			 3.455
	Polarity score:			 7
	Charge score :			 -3
	Proportion of polar atoms: 	39.535
	Alpha sphere density : 		5.340
	Cent. of mass - Alpha Sphere max dist: 13.342
	Flexibility : 			 0.189

Pocket 2 :
	Score : 			28.999
	Druggability Score : 		0.023
	Number of Alpha Spheres : 	71
	Total SASA : 			280.102
	Polar SASA : 			166.585
	Apolar SASA : 			113.517
	Volume : 			605.542
	Mean local hydrophobic density : 15.176
	Mean alpha sphere radius :	3.722
	Mean alp. sph. solvent access :  0.564
	Apolar alpha sphere proportion : 0.239
	Hydrophobicity score:		3.000
	Volume score: 			 4.167
	Polarity score:			 7
	Charge score :			 -1
	Proportion of polar atoms: 	43.902
	Alpha sphere density : 		4.995
	Cent. of mass - Alpha Sphere max dist: 9.773
	Flexibility : 			 0.484

Pocket 3 :
	Score : 			24.387
	Druggability Score : 		0.590
	Number of Alpha Spheres : 	54
	Total SASA : 			216.441
	Polar SASA : 			88.116
	Apolar SASA : 			128.325
	Volume : 			476.675
	Mean local hydrophobic density : 33.263
	Mean alpha sphere radius :	3.718
	Mean alp. sph. solvent access :  0.457
	Apolar alpha sphere proportion : 0.704
	Hydrophobicity score:		46.500
	Volume score: 			 5.250
	Polarity score:			 4
	Charge score :			 4
	Proportion of polar atoms: 	31.250
	Alpha sphere density : 		3.994
	Cent. of mass - Alpha Sphere max dist: 11.273
	Flexibility : 			 0.351

Pocket 4 :
	Score : 			24.301
	Druggability Score : 		0.633
	Number of Alpha Spheres : 	44
	Total SASA : 			28.461
	Polar SASA : 			4.308
	Apolar SASA : 			24.153
	Volume : 			122.600
	Mean local hydrophobic density : 38.300
	Mean alpha sphere radius :	3.239
	Mean alp. sph. solvent access :  0.425
	Apolar alpha sphere proportion : 0.909
	Hydrophobicity score:		35.700
	Volume score: 			 4.000
	Polarity score:			 5
	Charge score :			 0
	Proportion of polar atoms: 	28.571
	Alpha sphere density : 		2.993
	Cent. of mass - Alpha Sphere max dist: 7.591
	Flexibility : 			 0.086

