Pocket 1 :
	Score : 			32.290
	Druggability Score : 		0.827
	Number of Alpha Spheres : 	64
	Total SASA : 			299.333
	Polar SASA : 			81.643
	Apolar SASA : 			217.690
	Volume : 			563.439
	Mean local hydrophobic density : 35.667
	Mean alpha sphere radius :	3.584
	Mean alp. sph. solvent access :  0.515
	Apolar alpha sphere proportion : 0.656
	Hydrophobicity score:		45.600
	Volume score: 			 4.900
	Polarity score:			 4
	Charge score :			 1
	Proportion of polar atoms: 	31.579
	Alpha sphere density : 		5.098
	Cent. of mass - Alpha Sphere max dist: 10.957
	Flexibility : 			 0.569

Pocket 2 :
	Score : 			28.886
	Druggability Score : 		0.272
	Number of Alpha Spheres : 	61
	Total SASA : 			217.660
	Polar SASA : 			107.766
	Apolar SASA : 			109.894
	Volume : 			416.191
	Mean local hydrophobic density : 28.812
	Mean alpha sphere radius :	3.433
	Mean alp. sph. solvent access :  0.435
	Apolar alpha sphere proportion : 0.525
	Hydrophobicity score:		32.917
	Volume score: 			 4.083
	Polarity score:			 3
	Charge score :			 2
	Proportion of polar atoms: 	35.135
	Alpha sphere density : 		4.665
	Cent. of mass - Alpha Sphere max dist: 12.378
	Flexibility : 			 0.539

Pocket 3 :
	Score : 			19.634
	Druggability Score : 		0.187
	Number of Alpha Spheres : 	36
	Total SASA : 			234.233
	Polar SASA : 			71.203
	Apolar SASA : 			163.030
	Volume : 			636.890
	Mean local hydrophobic density : 25.000
	Mean alpha sphere radius :	4.172
	Mean alp. sph. solvent access :  0.696
	Apolar alpha sphere proportion : 0.722
	Hydrophobicity score:		24.375
	Volume score: 			 4.625
	Polarity score:			 5
	Charge score :			 0
	Proportion of polar atoms: 	30.769
	Alpha sphere density : 		3.555
	Cent. of mass - Alpha Sphere max dist: 8.265
	Flexibility : 			 0.149

Pocket 4 :
	Score : 			17.435
	Druggability Score : 		0.050
	Number of Alpha Spheres : 	37
	Total SASA : 			251.510
	Polar SASA : 			144.031
	Apolar SASA : 			107.479
	Volume : 			635.790
	Mean local hydrophobic density : 15.333
	Mean alpha sphere radius :	3.967
	Mean alp. sph. solvent access :  0.520
	Apolar alpha sphere proportion : 0.486
	Hydrophobicity score:		8.750
	Volume score: 			 4.250
	Polarity score:			 6
	Charge score :			 2
	Proportion of polar atoms: 	48.000
	Alpha sphere density : 		4.336
	Cent. of mass - Alpha Sphere max dist: 10.715
	Flexibility : 			 0.514

