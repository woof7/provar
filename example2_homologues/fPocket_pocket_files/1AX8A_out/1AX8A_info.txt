Pocket 1 :
	Score : 			34.512
	Druggability Score : 		0.957
	Number of Alpha Spheres : 	83
	Total SASA : 			171.110
	Polar SASA : 			43.102
	Apolar SASA : 			128.009
	Volume : 			308.672
	Mean local hydrophobic density : 47.556
	Mean alpha sphere radius :	3.348
	Mean alp. sph. solvent access :  0.393
	Apolar alpha sphere proportion : 0.759
	Hydrophobicity score:		53.812
	Volume score: 			 4.188
	Polarity score:			 6
	Charge score :			 2
	Proportion of polar atoms: 	30.952
	Alpha sphere density : 		4.946
	Cent. of mass - Alpha Sphere max dist: 12.851
	Flexibility : 			 0.343

Pocket 2 :
	Score : 			17.467
	Druggability Score : 		0.075
	Number of Alpha Spheres : 	44
	Total SASA : 			190.617
	Polar SASA : 			51.740
	Apolar SASA : 			138.877
	Volume : 			500.182
	Mean local hydrophobic density : 20.727
	Mean alpha sphere radius :	3.814
	Mean alp. sph. solvent access :  0.555
	Apolar alpha sphere proportion : 0.500
	Hydrophobicity score:		19.800
	Volume score: 			 3.600
	Polarity score:			 6
	Charge score :			 -1
	Proportion of polar atoms: 	38.462
	Alpha sphere density : 		3.650
	Cent. of mass - Alpha Sphere max dist: 8.325
	Flexibility : 			 0.343

Pocket 3 :
	Score : 			14.227
	Druggability Score : 		0.067
	Number of Alpha Spheres : 	41
	Total SASA : 			210.635
	Polar SASA : 			122.478
	Apolar SASA : 			88.157
	Volume : 			512.179
	Mean local hydrophobic density : 0.000
	Mean alpha sphere radius :	4.407
	Mean alp. sph. solvent access :  0.626
	Apolar alpha sphere proportion : 0.024
	Hydrophobicity score:		25.364
	Volume score: 			 3.909
	Polarity score:			 6
	Charge score :			 0
	Proportion of polar atoms: 	48.000
	Alpha sphere density : 		3.155
	Cent. of mass - Alpha Sphere max dist: 7.579
	Flexibility : 			 0.266

Pocket 4 :
	Score : 			14.101
	Druggability Score : 		0.314
	Number of Alpha Spheres : 	42
	Total SASA : 			262.414
	Polar SASA : 			69.193
	Apolar SASA : 			193.221
	Volume : 			596.767
	Mean local hydrophobic density : 27.533
	Mean alpha sphere radius :	3.824
	Mean alp. sph. solvent access :  0.600
	Apolar alpha sphere proportion : 0.714
	Hydrophobicity score:		50.700
	Volume score: 			 4.300
	Polarity score:			 4
	Charge score :			 0
	Proportion of polar atoms: 	32.143
	Alpha sphere density : 		4.191
	Cent. of mass - Alpha Sphere max dist: 10.679
	Flexibility : 			 0.427

