%% Read PDB ATOM file
% Uses 3rd party PDBRead module 
% This is a slow but robust method - use fn_PDBFastLoad for large CONCOORD
% sets - if one loads all will!

function [status,pdb]=fn_PDBFullRead(pdb,pdbFullFile,index)
    %disp(strcat('START: fn_PDBFullRead(pdb,pdbFullFile,index) / ',pdbFullFile));

    % Wrapper for single or multi-file pass...
    if ischar(pdbFullFile)
        % Single file passed
        [status,pdb,atom_start]=fn_PDBFullRead_Sub(pdb, pdbFullFile, index, 1);
        
    else
        % For pocket prediction pdbs consisting of  seperate files in a directory we
        % need to load them all...
        thispdb=pdb;
        atom_start=1;
       
        for iLoop = 1:1:(size(pdbFullFile,1))
            % List passed (e.g. fPocket files in dir)
            thisFile=strcat(pdbFullFile(1).dir,pdbFullFile(iLoop).name);
            [status,thispdb,atom_start]=fn_PDBFullRead_Sub(thispdb,thisFile,index,atom_start);
            
        end
        pdb=thispdb;
    end
end

function [status,pdb,atom_start]=fn_PDBFullRead_Sub(pdb,pdbFullFile,index,atom_start)
    
    status='';
    try
        PDB_struct = PDBRead(pdbFullFile);
        noatoms=size(PDB_struct.ATOM,2);
        
        % Use to detect residue change to build up AA sequence string
        thisResNo=0;
        aaSeq='';
        
        % Need in matrix forms...
        atom_index=0;   % Indexes the PDB_struct array
        for atLoop=atom_start:1:(noatoms+atom_start-1)
            atom_index=atom_index+1;
            %xyz coords
            xyz{atom_index}=[PDB_struct.ATOM(1,atom_index).X PDB_struct.ATOM(1,atom_index).Y PDB_struct.ATOM(1,atom_index).Z];

            % extract fields
            atomid{atom_index}=PDB_struct.ATOM(1,atom_index).AtomSerNo;
            resno{atom_index}=PDB_struct.ATOM(1,atom_index).resSeq;
            residue{atom_index}=PDB_struct.ATOM(1,atom_index).resName;
            chain{atom_index}=PDB_struct.ATOM(1,atom_index).chainID;
            atomtype1{atom_index}=PDB_struct.ATOM(1,atom_index).AtomName;        
            atomtype2{atom_index}=PDB_struct.ATOM(1,atom_index).element;

            if ~(resno{atom_index}==thisResNo)
                %aaSeq=strcat(aaSeq,residue{atLoop});           
                aaSeq=strcat(aaSeq,fn_Provar_getAminoLetter(residue{atom_index}));
                thisResNo=resno{atom_index};
            else
                
            end
        end
        
        % Need an array of number of amino acids in each chain
        %[chainids, chainarray, chainstart]=fn_getchains(chain(atom_start:end),resno(atom_start:end));
        [chainids, chainarray, chainstart]=fn_getchains(chain,resno);
        pdb.chainIDs{index}=chainids;
        pdb.chainarray{index}=chainarray;
        pdb.chainstart{index}=chainstart;

        % Convert cell xyz to double matrix 
        xyz=cell2mat(xyz');

        % Transpose and save into handles...
        if isfield(pdb,'xyz')
            pdb.xyz{index}=vertcat(pdb.xyz{index},xyz); 
        else
            pdb.xyz{index}=xyz;
        end
           
        if isfield(pdb,'atomid')
            pdb.atomid{index}=vertcat(pdb.atomid{index},atomid');
        else
            pdb.atomid{index}=atomid';
        end
            
        if isfield(pdb,'residue')
            pdb.residue{index}=vertcat(pdb.residue{index},residue');
        else
            pdb.residue{index}=residue';
        end
       
        if isfield(pdb,'chain')
            pdb.chain{index}=vertcat(pdb.chain{index},chain');
        else
            pdb.chain{index}=chain';
        end
        
        if isfield(pdb,'resno')
            pdb.resno{index}=vertcat(pdb.resno{index},resno');
        else
            pdb.resno{index}=resno';
        end
        
        if isfield(pdb,'atomtype1')
            pdb.atomtype1{index}=vertcat(pdb.atomtype1{index},atomtype1');
        else
            pdb.atomtype1{index}=atomtype1';
        end
        
        if isfield(pdb,'atomtype2')
            pdb.atomtype2{index}=vertcat(pdb.atomtype2{index},atomtype2');
        else
            pdb.atomtype2{index}=atomtype2';
        end
        
        if isfield(pdb,'filename')
            pdb.filename{index}=horzcat(pdb.filename{index},strcat(';',pdbFullFile));
        else
           pdb.filename{index}= pdbFullFile;
        end
        
        %pdb.pdbid{index}=upper(pdbid);
        %pdb.ccid{index}=ccid;
        if isfield(pdb,'aaseq')
            pdb.aaseq{index}=horzcat(pdb.aaseq{index},aaSeq);
        else
            pdb.aaseq{index}=aaSeq;
        end
             
        atom_start=atLoop;
        
        try
            pdb.code=PDB_struct.HEADER.idCode;
        catch
            pdb.code='XX Non-protein PDB read...';
        end
    catch
        err=lasterror;
        status=err.message;
    end
    
    %disp(strcat('END: fn_PDBFullRead(pdb,pdbFullFile,index) / ',pdbFullFile));
   %disp('---------');
    
end
