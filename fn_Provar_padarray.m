%% Pad arrays - used for sets of homologues or multi structs where number
%  of aminos differs between structures

function [leftarray,rightarray]=fn_Provar_padarray(leftarray, rightarray)

    leftsize=size(leftarray,2);
    rightsize=size(rightarray,2);

    diff=abs(leftsize-rightsize);
    
    if diff>0
        padding(diff)=0;
        
         if leftsize>rightsize
             rightarray=[rightarray padding];
             
         else
             leftarray=[leftarray padding];
             
         end
    end
    
   
        
    
end


