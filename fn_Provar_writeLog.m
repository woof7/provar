function [params]=fn_Provar_writeLog(params)
% This is a mini-log file just containing folder locations & stats etc
% (There is also a full MATLAB diary file containing all output)

    params.status.fn_Provar_writeLog='OK';
     try
        % open log file
        fid=fopen(fullfile(params.provar_dir,'Provar_summary.txt'),'wt');


        fprintf(fid,'%s\n','************');  
        fprintf(fid,'%s\n',strcat('Provar v',params.provar_version));
        fprintf(fid,'%s\n','************');

        fprintf(fid,'%s\n',strcat('Provar run ID : ',params.pdfid));
        fprintf(fid,'%s\n',strcat('Type          : ',params.run_type));
        fprintf(fid,'%s\n',strcat('Structure dir : ',params.structure_dir));
        fprintf(fid,'%s\n',strcat('Reference PDB : ',params.ref_pdb));

        for iLoop=1:1:(params.pocketProgNo)
            run_status='..skip';
            if params.progrun{iLoop}==1
                run_status='..run'; 
            end
            fprintf(fid,'%s\n',strcat('Pocket [',num2str(iLoop),']/ ',params.pocketProg{iLoop},': ',params.progid{iLoop},run_status));
        end

        if strcmp(params.run_type,'HOM')
            fprintf(fid,'%s\n',strcat('Alignment file: ',params.seq_align_file));
        end
        fprintf(fid,'%s\n',strcat('Group         : ',params.groupid));
        fprintf(fid,'%s\n',strcat('tCONCOOORD ID : ',params.cc_id));
        fprintf(fid,'%s\n',strcat('Dataset ID    : ',params.datasetid));
        %fprintf(fid,'%s',strcat('FASTA?        : ',num2str(params.useFASTA)));

        fprintf(fid,'%s\n','------------');  
        fprintf(fid,'%s\n','Statistics  '); 
        fprintf(fid,'%s\n','These are quartiles Q1(25%), Q2(median) and Q3(75%)  '); 
        fprintf(fid,'%s\n','For each pocket program run  '); 

        for iLoop=1:1:(params.pocketProgNo)
            if params.progrun{iLoop}==1
                fprintf(fid,'%s\n',strcat('Program         : ',params.pocketProg{iLoop}));
                if ~strcmp(params.run_type,'HOM')
                    fprintf(fid,'%s\n',strcat('atom            :', params.stats{iLoop}.atom));
                    fprintf(fid,'%s\n',strcat('atom normalised :', params.stats{iLoop}.atom_norm ));
                else
                    fprintf(fid,'%s\n',strcat('* Homologous stats based on distribution of probabilities against all FASTA global align positions *'));
                end
                fprintf(fid,'%s\n',strcat('amino           :', params.stats{iLoop}.amino));
                fprintf(fid,'%s\n',strcat('amino avg       :', params.stats{iLoop}.amino_avg));
                fprintf(fid,'%s\n','------------');  
            end
        end
     catch
        err=lasterror;
        params.status.fn_Provar_writeLog=strcat('Error: fn_Provar_writeLog: error writing log file: ',err.message); 
    end
    % close log
    fclose(fid);
    
end
