%% For sequence alignment index conversions between ClustalW alignments and
%  the PDB residue numbers 

% 20/05/2011
% This fn is called from fn_Provar_align_helper if params.mustang_align==1

function [clustalref_to_pdbres, pdbres_to_clustalref]=fn_Provar_align_helper_mustang(params,pdb_struct)

    clustalref_to_pdbres='';
    pdbres_to_clustalref='';

    if size(pdb_struct.chainIDs,1)>1
        disp('Error: fn_Provar_align_helper: more than one chain passed into helper...');
        return
    end

    chainid=pdb_struct.chainIDs{1};
    pdbcode=pdb_struct.code;
    

    fid=fopen(params.seq_align_file);
    seq_align=textscan(fid,'%s %s','delimiter',',');

    
    % How many aligments?
    no_aligns=size(seq_align{1},1);
    iAlignRef=0;
    for iAlignLoop=1:1:no_aligns
        thispdb=seq_align{1}{iAlignLoop};
        if strcmp(thispdb,strcat(pdbcode,'_',chainid))
            iAlignRef=iAlignLoop;
            break;
        end
    end

    if iAlignRef==0
        disp('Error: fn_Provar_align_helper: cannot find pdb in Clustal file...');
        return
    end

    aligned_seq=seq_align{2}{iAlignRef};
    % This translates a clustal position to residue pos...
    clustalref_to_pdbres=zeros(size(aligned_seq,2),1);
    % This translates a residue pos to a clustal pos...
    pdbres_to_clustalref=zeros(pdb_struct.chainarray{1} + pdb_struct.chainstart{1} - 1,1);

    % Loop PDB file
    prevresno=0;
   
    %seq_start_idx=pdb_struct.chainstart{1};   % The PDB start pos
    seq_start_idx=1; 
    non_gap_start=regexp(aligned_seq,'[^-]', 'once' );  % 1st non-gap in alignment
    seq_start_idx=seq_start_idx + non_gap_start -1;

    %
    match_idx=1;
    no_atoms=size(pdb_struct.xyz{1},1);
    for iAtLoop=1:1:no_atoms
        thisresno=pdb_struct.resno{1}{iAtLoop};
        if thisresno==prevresno
            % ignore

        else
            % Get amino code for this residue
            amino_code=fn_Provar_getAminoLetter(pdb_struct.residue{1}{iAtLoop});

            % 09/05/10 - Adding one each time causes spurious matches
            % (as per start) if there are breaks in PDB seq... Need to
            % add on differentce in this/prev resnos (except at start
            % which is dealt with above) to skip matching
            % aminos in alignment but not in the PDB file.

            if prevresno == 0
                actual_idx=seq_start_idx;
            else
                %seq_start_idx=actual_idx + (thisresno - prevresno);
                seq_start_idx=actual_idx + 1;
            end

            % Find match in Clustalseq
            align_subseq=aligned_seq(seq_start_idx:end);

            match_idx=regexp(align_subseq,amino_code,'once');

            % actual index of aa from start (not just from subsequence start)
            actual_idx=match_idx + seq_start_idx-1;

            if ~isempty(match_idx)
                % actual_idx=match_idx + seq_start_idx-1;
                pdbres_to_clustalref(thisresno)=actual_idx;
                clustalref_to_pdbres(actual_idx)=thisresno;

                %seq_start_idx=actual_idx+1;  * See note above
            end

            prevresno=thisresno;
        end

    end

end

