function params=fn_Provar_write_pdb_files(params, pocketProgIdx)
   % Writes set of PDB files for a given program
   % For each we have:
   % 1) PDB with Atom-based scores in B-factor
   % 2) PDB with amino-acid scores 
   % 3) PDB with amino-averaged scores
   
   % Note that the reference PDB is referenced in two ways: 1) Using a file
   % handle (fpdb_in) to handle low/level read/write of PDB lines and
   % updates on ATOM lines and 2) as a struct (ref_structure) to allow
   % correct cross referencing of the atom and residue numbering.
   
   params.status.fn_Provar_write_pdb_files='OK';
   try 
       
        % For the PDB file with temp factors updated with atom-based
        % probabilities (not valid for homologues)
        if ~strcmp(params.run_type,'HOM')
            pdbout_atom=strcat(params.pdfid,'_',params.pocketProg{pocketProgIdx},'_atom_out_',sprintf('%4.2f',params.pocketradius),'A.pdb');
            pdbout_atom=fullfile(params.provar_dir,params.pocketProgAbbrev{pocketProgIdx},pdbout_atom);
        end
        % For the PDB file with temp factors updated with amino-based
        % probabilities
        pdbout_amino=strcat(params.pdfid,'_',params.pocketProg{pocketProgIdx},'_amino_out_',sprintf('%4.2f',params.pocketradius),'A.pdb');
        pdbout_amino=fullfile(params.provar_dir,params.pocketProgAbbrev{pocketProgIdx},pdbout_amino);
        
        % For the PDB file with temp factors updated with
        % amino-average-based probabilities
        pdbout_amino_avg=strcat(params.pdfid,'_',params.pocketProg{pocketProgIdx},'_amino_avg_out_',sprintf('%4.2f',params.pocketradius),'A.pdb');
        pdbout_amino_avg=fullfile(params.provar_dir,params.pocketProgAbbrev{pocketProgIdx},pdbout_amino_avg);
       
        
        % Read the reference PDB in using standard method - need this to
        % work out correct atom/amino numberings...
        [ref_structure,conformer_no,params] = fn_Provar_loadStructure(params,params.ref_pdb);
        no_atoms=size(ref_structure.atomid{1},1);
        
        if ~strcmp(params.status.fn_Provar_loadStructure,'OK')
            disp(params.status.fn_Provar_loadStructure);
        end
        
        % Reference PDB file to read
        fpdb_in=fopen(params.ref_pdb,'r');
        
        % Output new files...
        if ~strcmp(params.run_type,'HOM')
            fpdb_out_atom=fopen(pdbout_atom,'w');
        end
        fpdb_out_amino=fopen(pdbout_amino,'w');
        fpdb_out_amino_avg=fopen(pdbout_amino_avg,'w');
        
        % Read PDB lines
        end_of_file=false;
        while (end_of_file==false)
            line=fgets(fpdb_in);
            if isnumeric(line)
                end_of_file=true;
            else
                
                if strcmp(line(1:4),'ATOM')
                    
                    % Extract the atom number from the ref PDB
                    this_atom_no=str2num(line(7:11));
                    
                    if ~strcmp(params.run_type,'HOM')
                        % Use the Provar atom probabilities saved into
                        % params.p....
                        this_atom_p=params.p.provar_atom_p{pocketProgIdx}(this_atom_no);

                        % Update the temp fac (atom)
                        line(61:66)=sprintf('%6.2f',100*this_atom_p);
                        % Write the atom line to new PDB
                        fprintf(fpdb_out_atom,'%s',line);
                    end

                    % Get residue number from ref PDB in struct 
                    this_resno=ref_structure.resno{1}{this_atom_no};
                    
                    % We can get the Provar amino and amino_avg
                    % probabilities from params.p.... and write here
                    this_res_p=params.p.provar_amino_p{pocketProgIdx}(this_resno);
                    % Update the temp fac (amino)
                    line(61:66)=sprintf('%6.2f',100*this_res_p);
                    % Write the amino line to new PDB
                    fprintf(fpdb_out_amino,'%s',line);
                    
                    % Update the temp fac (amino_avg)
                    this_res_p_avg=params.p.provar_amino_avg_p{pocketProgIdx}(this_resno);
                    line(61:66)=sprintf('%6.2f',100*this_res_p_avg);
                    % Write the amino avg line to new PDB
                    fprintf(fpdb_out_amino_avg,'%s',line);
                    
                    
                elseif strcmp(line(1:4),'HEAD')
                    
                    % Put user text and Provar version into HEADER
                    line=['HEADER    '  params.pdb_ref];
                    line=sprintf('%s\n',line);
                    
                    % Write the HEADER to new PDBs
                    if ~strcmp(params.run_type,'HOM')
                        fprintf(fpdb_out_atom,'%s',line);
                    end
                    fprintf(fpdb_out_amino,'%s',line);
                    fprintf(fpdb_out_amino_avg,'%s',line);
                    
                else
                      % Write non HEADER/ATOM line to new PDBs
                      if ~strcmp(params.run_type,'HOM')
                        fprintf(fpdb_out_atom,'%s',line);
                      end
                     fprintf(fpdb_out_amino,'%s',line);
                     fprintf(fpdb_out_amino_avg,'%s',line);  
                end
                  
            end
        end
        
        try
            % Close handles
            close(fpdb_in);
            if ~strcmp(params.run_type,'HOM')
                close(fpdb_out_atom);
            end
            close(fpdb_out_amino);
            close(fpdb_out_amino_avg);
        catch
        end
   catch
       err=lasterror;
       params.status.fn_Provar_write_pdb_files=strcat('Error: fn_Provar_write_pdb_files:',err.message);
       
   end  
    
end
