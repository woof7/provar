%% Write out probability and b-factor files
function params=fn_Provar_write_pb_files(params,provar_array,type,progid,isATOM)
    % type: b - bfactors
    %       p - probabilities
    
    % progid: 1 - PASS
    %         2 - LIGSITE
    
    % isATOM: 0 - amino
    %         1 - ATOM
    %         2 - amino by ATOM (average of ATOMS grouped a RES level)
    %             Globals for homologues (i.e. for every clustal allignment position not
    %             based on reference structure
    %         3 - Global alignement (amino) 
    %         4 - Global alignment  (res avg)
    %         5 - Global aligned onto ref (amino)
    %         6 - Global aligned onto ref (res avg)
    %         7 - Stddev of global avg onto ref 
    params.status.fn_Provar_write_pb_files='OK';
    try
        idx=[1:size(provar_array{progid},2)];
        switch isATOM
            case 0
                fileroot=strcat(params.pdfid,'_',params.pocketProg{progid},'_',type,'_amino_out_',sprintf('%4.2f',params.pocketradius),'A-radial');
            case 1
                fileroot=strcat(params.pdfid,'_',params.pocketProg{progid},'_',type,'_ATOM_out_',sprintf('%4.2f',params.pocketradius),'A-radial');
            case 2 
                fileroot=strcat(params.pdfid,'_',params.pocketProg{progid},'_',type,'_amino_avg_by_atom_out_',sprintf('%4.2f',params.pocketradius),'A-radial');
            case 3
                fileroot=strcat(params.pdfid,'_',params.pocketProg{progid},'_',type,'_global_alignment_amino_out_',sprintf('%4.2f',params.pocketradius),'A-radial');
            case 4
                fileroot=strcat(params.pdfid,'_',params.pocketProg{progid},'_',type,'_global_alignment_avg_out_',sprintf('%4.2f',params.pocketradius),'A-radial');
            case 5
                fileroot=strcat(params.pdfid,'_',params.pocketProg{progid},'_',type,'_global_alignment_amino_out_REF_',params.pdb_ref,'_',sprintf('%4.2f',params.pocketradius),'A-radial');
            case 6
                fileroot=strcat(params.pdfid,'_',params.pocketProg{progid},'_',type,'_global_alignment_avg_out_REF_',params.pdb_ref,'_',sprintf('%4.2f',params.pocketradius),'A-radial');
            case 7
                fileroot=strcat(params.pdfid,'_',params.pocketProg{progid},'_',type,'_global_alignment_avg_STDDEV_REF_',params.pdb_ref,'_',sprintf('%4.2f',params.pocketradius),'A-radial');
        end

        filename=strcat(params.provar_dir,'/',params.pocketProgAbbrev{progid},'/',fileroot);
        %% TEST ONLY filename=strcat(params.provar_dir,'T14100/',params.pocketProgAbbrev{progid},'/',fileroot);
        
        values_out=horzcat(idx', provar_array{progid}');
        dlmwrite(strcat(filename,'.txt'),values_out);
    catch
        err=lasterror;
        params.status.fn_Provar_write_pb_files=err.message;
    end
    
end