%% Read FASTA file and convert 
% 26/05/2011

function [seq_align]= fn_Provar_readFASTA(params,fastaFile)
    
    % Get this PDB's sequence alignment from file
    fid=fopen(fastaFile);
    fasta=textscan(fid,'%s %s','delimiter',',');
    fasta=fasta{1};
    
    iPDBno=0;
    
    seq_align=cell(1,2);
    
    for iLineLoop=1:1:size(fasta,1)
        thisLine=fasta{iLineLoop};
        
        % New sequence 
        if strcmp(thisLine(1,1),'>')
            
            % Extract PDB and chain
            pdbid=thisLine(2:5);
            
            thisLine_remainder=thisLine(6:end);
            
            % Find delimiter (if one)
            delimpos=regexp(thisLine_remainder,'\W','once');
            if ~isempty(delimpos) && delimpos>1
                get_chain=regexp(thisLine_remainder(1:delimpos-1),'[A-Za-z0-9]','once');
                if ~isempty(get_chain)
                    chainid=thisLine_remainder(get_chain);
                else
                    chainid='';
                end
            else
                 chainid='';
                 
            end
         
            
            % PDB ID
            iPDBno=iPDBno+1;
            seq_align{1,1}{iPDBno,1}=strcat(pdbid,chainid);
            seq_align{1,2}{iPDBno,1}='';
        else
            seq_align{1,2}{iPDBno,1}=strcat(seq_align{1,2}{iPDBno,1},thisLine);
        end
        
    end
     
end