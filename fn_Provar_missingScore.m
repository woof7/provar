%% Calculate missing residue scores from set of Clustal alignment files

function [missingScores]=fn_Provar_missingScore(params)

    missingScores='';

    % Read in the structure alignment file
    try
        
          % Get this PDB's sequence alignment from file
        % FASTA or modified format?
        if params.useFASTA==1
            seq_align= fn_Provar_readFASTA(params,params.seq_align_file);
        
        else
            fid=fopen(params.seq_align_file);
            seq_align=textscan(fid,'%s %s','delimiter',',');
        end
        
        %fid=fopen(params.seq_align_file);
        %seq_align=textscan(fid,'%s %s','delimiter',',');

        % How many aligments?
        no_pdb=size(seq_align{1},1);

%         % Should match number of files!
%         if ~(no_pdb==iFileSeq)
%             disp('Error:  fn_Provar_homologues: no of PDBs in alignment file does not match number read...');
%             return
%         end

        % No aminos
        no_aa=size(seq_align{2}{1},2);

        % This array has the PDB count which we can deduct 1 from for each
        % non-matched sequence
        aa_totals=ones(1,no_aa);
        aa_totals=aa_totals*no_pdb;

        % Go through each PDB seq...
        for iLoop=1:1:no_pdb
            this_pdbchain=strrep(seq_align{1}{iLoop},'_','');
            
            [skipFlag,no_pdb]=homol_skip(params,this_pdbchain, no_pdb);
            
            if skipFlag==0
                this_prot=seq_align{2}{iLoop};
                % find any missings "-"
                missing_index=find(this_prot=='-');

                % Subtract from totals
                aa_totals(missing_index)=aa_totals(missing_index)-1;
                
            else
               % Just one less PDB struct, so reduce all counts by 1 (irrespective of skipped's align) 
               aa_totals=aa_totals-1;
            end

        end
        missingScores=aa_totals;

    catch
        disp(strcat('Error: fn_Provar_missingScore: problem loading sequence alignment file - aborting...',''));
        return
    end
end

function [skipFlag, no_pdb]=homol_skip(params,pdbchain, no_pdb)
    % Some PDBs shouldn't be included in calcs - disregard when calculating number
    % of aligned residues.  E.g. we have a holo structure in apo set for
    % alignment purposes only.
    skipFlag=0;
    
    for skipLoop=1:1:size(params.homSkip,2)
        skippdbchain=params.homSkip{skipLoop};
        if strcmp(skippdbchain,pdbchain)
            skipFlag=1;
            no_pdb=no_pdb-1;
        end
        
    end 
    
end

