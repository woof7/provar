%% Creates required directory structure under the Provar (Pnnnnn) folder.
function params=fn_Provar_create_directories(params)

    params.status.fn_Provar_create_directories='OK';
    
    disp('> Creating Provar directory structure...');
    % Change to Provar base directory
    try
        cd(params.base_dir);
    catch
        % Fail if can't find
        params.status.fn_Provar_create_directories=strcat('Error: Provar base directory (',params.base_dir,') does not exist. ');
    	return;
    end
        
    % Make Provar sub dir
    try
        disp(strcat('> Creating Provar run-specific sub directory: ',params.pdfid));
        [s,mess,messid]=mkdir(params.pdfid); 
        if strcmp(messid,'MATLAB:MKDIR:DirectoryExists')
            disp('..exists');
        end
    catch
        params.status.fn_Provar_create_directories=strcat('Error: Unable to create sub directory: ',params.pdfid);
        return;
    end
        
    % The provar directory will contain the main outputs (probability
    % files, pdb files etc...)
    params.provar_dir=fullfile(params.base_dir,params.pdfid);
    
    % Change to Provar sub dir    
    try
       cd(params.provar_dir); 
    catch
       params.status.fn_Provar_create_directories=strcat('Error: Unable to change to Provar sub directory: ',params.pdfid);
       return;
    end
    
    % Create pocket program directories
    for iLoop=1:1:params.pocketProgNo
    
        if params.progrun{iLoop}==1
           try
               % Requested programs - PASS, LIGSITE etc
               disp(strcat('> Creating sub directory for: ',params.pocketProg{iLoop} )); 
               [s,mess,messid]=mkdir(params.pocketProgAbbrev{iLoop});
                if strcmp(messid,'MATLAB:MKDIR:DirectoryExists')
                    disp('..exists');
                end
           catch
                params.status.fn_Provar_create_directories=strcat('Error: Unable to create pocket program sub directory for:',params.pocketProg{iLoop});
                return;
           end
        end
    end
    
    % Create ref dir
%     try
%         disp('> Creating reference directory');
%         [s,mess,messid]=mkdir('ref');
%         if strcmp(messid,'MATLAB:MKDIR:DirectoryExists')
%             disp('..exists');
%         end
%     catch
%         disp(strcat('Error: Unable to create sub directory'));
%         return;
%     end


end


