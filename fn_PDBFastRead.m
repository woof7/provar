function [status,pdb]=fn_PDBFastRead(pdb,pdbFullFile,index)
    
    disp(strcat('START: fn_PDBFastRead(pdb,pdbFullFile,index) / ',pdbFullFile));
    status='';
    try
    [dummy1, atomid, atomtype1, residue, chain, resno, x, y, z, num1, num2, atomtype2]=...
                    textread(pdbFullFile,'%s %s %s %s %c %u %f %f %f %f %f %s');
         % Concat coordinates
         pdb.xyz{index}=[x y z];
         pdb.atomid{index}=atomid;
         pdb.residue{index}=residue;
         pdb.chain{index}=chain;
         pdb.resno{index}=resno;
         pdb.atomtype1{index}=atomtype1;
         pdb.atomtype2{index}=atomtype2;
         pdb.filename{index}=pdbFullFile;
         
         % PDB ID
         try
             % Get the file name
             pdb_file_name=regexp(pdbFullFile,'\/[0-9]\w{3}.*\Wpdb$','match');
             % Get PDB IDs
             pdbids=regexp(pdb_file_name,'[0-9]\w{3}','match');
             % Maybe of form:  '/1A6U_L_1A6W_H_1A6W_fs.pdb' so get last PDB ID!
             pdbids=pdbids{1};
             pdbid=pdbids(end);
             
         catch
             pdbid='????';
         end
        
         pdb.pdbid{index}=upper(pdbid);
    
    catch
        err=lasterror;
        status=err.message;
        
    end
%          % Need an array of number of amino acids in each chain
%          [chainids, chainarray, chainstart,handles]=fn_getchains(hObject,eventdata,handles,chain',resno');
%          pdb.chainIDs{index}=chainids;
%          pdb.chainarray{index}=chainarray;
%          pdb.chainstart{index}=chainstart;

    disp(strcat('END: fn_PDBFastRead(pdb,pdbFullFile,index) / ',pdbFullFile));
end

