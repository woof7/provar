%% Provar: Load a single pocket prediction dataset in (x,y,z) format

function [pocket_single,params]=fn_Provar_loadPocketPredictionSingle(params,struct_filename,conformer_idx)

    % Note: conformer_idx is the PDB/Chain if a homologous run
    
    params.status.fn_Provar_loadPocketPredictionSingle='OK';
    
    % pocket_single{iPocketProgLoop}.status  codes:
    % -1: N/A (null)
    % 0: OK
    % 1: Invalid file name
    % 2: PDB read failed
    
    pocket_single{params.pocketProgNo}='';

    % Get the fileame based on run type
    if ~isempty(struct_filename)

        % Load each PASS, LIGSITE etc matching (x,y,z) data
        for iPocketProgLoop=1:1:(params.pocketProgNo)
            
             pocket_single{iPocketProgLoop}.status=-1;

            % try and load the relevant pocket predictions for this
            % conformer
            switch params.run_type
                case 'CONF'
                    % Extract file / path / extension
                    [pathstr, filename, ext, versn] = fileparts(struct_filename);
                    
                    pocket_filename=strcat(filename,params.pocketProgFileSuffix{iPocketProgLoop});
                   % pocket_filename=strcat(pocket_filename,ext);

                case 'TC'
                    pocket_filename=strcat('tdisco',num2str(conformer_idx),params.pocketProgFileSuffix{iPocketProgLoop});

                case 'HOM'
                    % We have PDB ID not conformer number
                    pocket_filename=strcat(conformer_idx,params.pocketProgFileSuffix{iPocketProgLoop});

                case 'ROT'
                    % Rotations or other sequenced 'Rnn'
                    pocket_filename=strrep(struct_filename,'.pdb','');
                    pocket_filename=strcat(pocket_filename,params.pocketProgFileSuffix{iPocketProgLoop});
                    
                case 'CC'
                    pocket_filename=strrep(struct_filename,'.pdb','');
                    pocket_filename=strcat(pocket_filename,params.pocketProgFileSuffix{iPocketProgLoop});

                otherwise
                    pocket_filename='';
                    pocket_single{iPocketProgLoop}.status=1;
            end

            % Now load whichever pocket prog data we have
            if params.progrun{iPocketProgLoop}==1

                pocket_filename=fullfile(params.progid{iPocketProgLoop},pocket_filename);
                
                disp(strcat('> Reading pocket filename: ', pocket_filename));
                % If pocket preds are each in sep files within a directory
                % get all individual .pdbs...
                if isdir(pocket_filename)
                    pocket_dir=pocket_filename;
                    pocket_filename = dir(strcat(pocket_filename,'*.pdb'));
                    pocket_filename(1).dir=pocket_dir;   % We need reference to the pocket dir in fn_PDBFullRead
                end
                
                [status_code,pocket_single{iPocketProgLoop}]=fn_PDBFullRead('',pocket_filename,1);

                if ~isempty(status_code)
                    pocket_single{iPocketProgLoop}.status=2;
                    
                   % errmsg=lasterr;
                    disp(strcat('Warning: Pocket file read error for :',params.pocketProg{iPocketProgLoop}));
                else
                   
                    pocket_single{iPocketProgLoop}.status=0;   % Specific prog status
                end

            else
                %disp(strcat('* Skipping pocket files of type: ', params.pocketProg{iPocketProgLoop}));

            end

        end

    else
        params.status.fn_Provar_loadPocketPredictionSingle='Error: fn_Provar_loadPocketPredictionSingle: Invalid structure file or conformer index';
  
    end

end
