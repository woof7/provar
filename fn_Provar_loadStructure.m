%% Provar - load a single PDB structure
function [structure,conformer_no,params] = fn_Provar_loadStructure(params,structure_file)
    
     % *** Load the CC pdb file ***
            params.status.fn_Provar_loadStructure='OK';
            structure='';
            conformer_no=0;
            try
                
                 % v4.7 If struct file contains absolute path (from base dir) then use it
%                 if ~strcmp(struct_file(1:6),'/Users')
%                      structure_file=strcat(params.base_dir,params.structure_dir,struct_file);                
%                 else
%                      structure_file=strcat(params.base_dir,struct_file); 
%                 end    
                % v4.81 - platform independent

                disp('');
                disp(strcat('> Reading structure filename: ', structure_file));
                [status_code,structure]=fn_PDBFullRead(structure,structure_file,1);
                if ~isempty(status_code)
                    errmsg=lasterr;
                  %  disp(strcat('Error: fn_Provar_loadStructure: PDB read fail -',errmsg));
                    
                    params.status.fn_Provar_loadStructure=strcat('Error: fn_Provar_loadStructure/fn_PDBFullRead:',structure_file,':',status_code);
                    disp(params.status.fn_Provar_loadStructure);
                    %status=status_code;
                end      
                
%                 % If error move on...
%                 if ~isempty(status_code)
%                    status(conformer_no).structure_set=status_code;
%                 else
%                    status(conformer_no).structure_set='';
%                 end
       
            catch
                
                err=lasterror;
                params.status.fn_Provar_loadStructure=strcat('Error: fn_Provar_loadStructure:',structure_file,':',err.message);
            end
end
