%% Provar: Get the index number from a file e.g. from CONCOORD, tCONCOORD
%  or NMR where files in numerical sequence

function [conformer_idx,params]=fn_Provar_getFileSequence(params,status,struct_filename)
    
    conformer_idx=-1;
    
    params.status.fn_Provar_getFileSequence='OK';
    try
        switch params.run_type
            
            case 'CONF'
                % Generic conformer (fine for tCONCOORD/CONCOORD)
                match=regexp(struct_filename,'[0-9]*.pdb','match');
                match=strrep(match,'.pdb','');
                conformer_idx=match;
                
            case 'CC'
                % CONCOORD
                match=regexp(struct_filename,'_H[0-9]*.pdb','match');
                match=strrep(match,'.pdb','');
                conformer_idx=strrep(match,'_H','');

            case 'TC'
                %tCONCOORD
                match=regexp(struct_filename,'tdisco[0-9]*.pdb','match');
                match=strrep(match,'.pdb','');
                conformer_idx=strrep(match,'tdisco','');

            case 'NMR'
                % NMR solution structures e.g. 2KWI_NMR_0.3.pdb
                match=regexp(struct_filename,'NMR_0.[0-9]*.pdb','match');
                match=strrep(match,'.pdb','');
                conformer_idx=strrep(match,'NMR_0.','');

            case 'ROT'
                % A random rotation set (i.e. just files with Rnn
                % sequence...)
                match=regexp(struct_filename,'_R[0-9]*.pdb','match');
                match=strrep(match,'.pdb','');
                conformer_idx=strrep(match,'_R','');
                
            otherwise
                % RAPIDO aligned format
                match=regexp(struct_filename,'_fs[0-9]*.pdb','match');
                match=strrep(match,'.pdb','');
                conformer_idx=strrep(match,'_fs','');
        end
        
        conformer_idx=str2double(conformer_idx{1});
    catch
        err=lasterror;
        params.status.fn_Provar_getFileSequence=strcat('Error: fn_Provar_getFileSequence:',struct_filename,':',err.message);
        
     

    end  
    
end
