%% Provar calling function
function Provar
    
    % Initialise
    params=fn_Provar_init();
    
    % Set-up user-defined parameters for directories, run IDs etc
   params=fn_Provar_params(params);
      
    cd(params.app_dir);
    try
        % Write command window output to a diary file in the Provar dircectory
        diary_file=fullfile(params.base_dir,strcat('Provar_',params.pdfid,'_MATLAB_',date,'.log'));
        diary(diary_file);
    catch
        disp(strcat('Cannot start diary file - check:',params.base_dir,'- this is the root directory for the Provar run...'));
    end
        
    disp('');
    disp('********************************************');
    disp(['      Provar (v' params.provar_version ') / Run ID : ' params.pdfid]);
    disp('********************************************');
    %disp(strcat('Provar run ID : ',params.pdfid));
        
    % Create the directory structure...
    params=fn_Provar_create_directories(params);
    if ~strcmp(params.status.fn_Provar_create_directories,'OK')
        disp(params.status.fn_Provar_create_directories);
        return;
    end
    
    cd(params.app_dir);
    
    disp('');
    disp(strcat('Type          : ',params.run_type));
    disp(strcat('Structure dir : ',params.structure_dir));
    disp(strcat('Reference PDB : ',params.ref_pdb));
    
    for iLoop=1:1:(params.pocketProgNo)
        run_status='..skip';
        if params.progrun{iLoop}==1
           run_status='..run'; 
        end
        disp(strcat('Pocket [',num2str(iLoop),']/ ',params.pocketProg{iLoop},': ',params.progid{iLoop},run_status));
    end
    disp(strcat('Alignment file: ',params.seq_align_file));
    
    disp(strcat('Group         : ',params.groupid));

    disp(strcat('tCONCOORD ID  : ',params.cc_id));
    disp(strcat('Dataset ID    : ',params.datasetid));
    
    %disp(strcat('FASTA?  : ',num2str(params.useFASTA)));
    

    disp('********************************************');
     disp('');
    
    %results='';
    
    switch params.run_type
        
        case 'HOM'         
            disp('Running Provar in homologues mode...');
            params=fn_Provar_homologues(params);
            disp(params.status.fn_Provar_homologues)
            if strcmp(params.status.fn_Provar_homologues,'OK')
                disp('');
                disp('Writing Provar log file for homologues');
                params=fn_Provar_writeLog(params);
                disp(params.status.fn_Provar_writeLog);
            end
            
        case {'TC','CONF','CC'}
              
             disp('Running Provar in multiple conformer mode...');
             params=fn_Provar_multiconfs(params);
             disp(params.status.fn_Provar_multiconfs);
             if strcmp(params.status.fn_Provar_multiconfs,'OK')
                disp('');
                disp('Writing Provar log file for multiple conformers');
                params=fn_Provar_writeLog(params);
                disp(params.status.fn_Provar_writeLog);
            end
              
        otherwise
            disp(strcat('Unrecognised run_type (',params.run_type,') - check parameters.'));
    end
    
    % Write the summary text file...
    
    disp('Finished');
    disp('--------------------------------------------');
    
    diary off;
end
    