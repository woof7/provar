% **************************************
% fn_Provar_params
% PROVAR PARAMETER FILE
% **************************************

% Set up user-parameters for Provar runs:
% Directory structures
% Which pocket programs
% Run IDs
% Alignment files (if nec)


function params=fn_Provar_params(params)
     
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  EXAMPLE 1 PARAMETERS FOR A SET OF CONFORMERS                         %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Example of a tCONCOORD-based run consisting of multiple conformers
    % each must have IDENTICAL atom/residue numbers
    % Simulations of apo Bcl-2  (ID:P00098)
    % *****************************************************************  
    % P00098:G00086/PA00123/LC00112
    
    % This is the provar run ID - A directory of this name gets created
    %under base_dir for all outputs.  It is also used to name PDB/logs
    params.pdfid='example1_output';

    % Specify whether conformers (CONF) or homologues (HOM)
    params.run_type='CONF';
   
    % BASE DIRECTORY
    % This is the root directory for all data and Provar outputs
    params.base_dir=fullfile(pwd,'example1_conformers');
      
    % Where's the reference structure?  Atom/amino-acid numbers use this
    % file.  In this case tCONCOORD returns ref.pdb as an atom-renumbered
    % form of the input structure
    params.ref_pdb=fullfile(params.base_dir,'ref.pdb');
    
    % Where are the PDB conformers to be found?
    params.structure_dir=fullfile(params.base_dir,'structures');

   % What is the pocket-prediction programs' root output dir?
   % (Optional - default is base_dir)
   params.run_dir=params.base_dir;
        
    % The directory for the PASS files
    params.pass_dir=fullfile(params.run_dir,'PASS_pocket_files');
    %params.generic_pocket_dir=fullfile(params.run_dir,'pocket_files');

    
    % This will append to the PDB HEADER in updated files
    % of form "Provar v4.81/......"
    % Keep to <30 chars or will truncate.
    params.pdb_ref='BCL-2 tCC using apo 1GJH';
 
    % Identifies data source - any reasonable non-null value is fine.
    params.datasetid='dataset_example1';
    % Used for grouping pocket prog predictions that all related to same
    % system.  Any non-null value such as 'test' will do.
    params.groupid='group_example1';
    % An identifier for the CONCOORD/tCONCOORD runs
    params.cc_id='TC_example1';

  
 
% ****************************************************************
% FINAL CONFIGURATION - AUTOMATIC DO NOT CHANGE
% ****************************************************************
  params.app_dir=pwd;

  % The descriptive names for pocket progs aren't
  % used within code as loop-based... translate here.
  if ~strcmp(params.pass_dir,'')
     params.progid{1}= params.pass_dir;
  end
  
  if ~strcmp(params.ligsite_dir,'')
     params.progid{2}= params.ligsite_dir;
  end
  
  if ~strcmp(params.sitemap_dir,'')
     params.progid{3}= params.sitemap_dir;
  end
  
  if ~strcmp(params.fpocket_vert_dir,'')
     params.progid{4}= params.fpocket_vert_dir;
  end
  
  % Direct fpocket (where they've given pocket-lining atoms)
  if ~strcmp(params.fpocket_dir,'')
     params.progid{5}= params.fpocket_dir;
  end
  
  if ~strcmp(params.fpocket_atom_dir,'')
     params.progid{6}= params.fpocket_atom_dir;
  end
  
  if ~strcmp(params.generic_pocket_dir,'')
     params.progid{7}= params.generic_pocket_dir;
  end
  
  if ~strcmp(params.generic_pocket_direct_dir,'')
     params.progid{8}= params.generic_pocket_direct_dir;
  end
 
  for iLoop=1:1:(params.pocketProgNo)
  
        % The run flags just simplify some of the coding...
      if ~strcmp(params.progid{iLoop},'')
        params.progrun{iLoop}=1;
      end
  end
  
  % For PDB HEADER updates (max 40 chars)
  params.pdb_ref=strcat('Provar v',params.provar_version,'/',params.pdb_ref);
  if length(params.pdb_ref)>40
     params.pdb_ref=params.pdb_ref(1:40); 
  end

  % Set the FASTA flag if we have an alignment file
  if ~strcmp(params.seq_align_file,'')
        params.useFASTA=1;
  end
  
       
end