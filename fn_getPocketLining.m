function [binary_amino, binary_atom, restally,resavg] = fn_getPocketLining(params, pdb, pdb_idx, pocket, direct)
   % PDF 4: Calculate, for a single protein chain, which aminos/atoms are
   % pocket lining.
   % Pocket lining is defined as within xA of any PASS/LIGSITE pocket point
   % where x is params.pocketradius
   
   % *****************************************************
   % 20/06/2010
   % v2 - hopefully much faster! - old version renamed _v1
   % *****************************************************
   
   % *****************************************************
   % 03/09/2010  (** NOT YET IMPLEMENTED RE: H - MOST DON'T HAVE **)
   % ** THE RADIUS IS SET IN setParams.m **
   % Pocket lining is now defined as per Davis & Sali (2010):
    %      "Binding sites were defined as residues 
    %       containing at least one non-hydrogen atom within 5 A˚ of the 
    %       interacting domain or peptide." 

    % Note: the radius is set in setParams.m so only need to change the
    % non-H atom part.

   % *****************************************************

   % 07/12/2011
   % Added direct mode where progs already state the pocket-lining atoms in
   % the file so provide option not to calculate directly
  
   
   % Loop all atoms in pdb
   no_atoms=size(pdb.xyz{pdb_idx},1);
   
   xyz=pdb.xyz{pdb_idx};
   resnos=pdb.resno{pdb_idx};
   atnos=pdb.atomid{pdb_idx};
   
   maxres=resnos(size(resnos,1));
   maxres=maxres{1};
   
   binary_amino(resnos{end})=0;
   binary_atom(no_atoms)=0;
   restally(maxres)=0;
   resavg(maxres)=0;
  
   pocket_xyz=pocket.xyz{1};
   
   % Sorted pocket atoms
   if direct==1
        pocket_atom_sorted=pocket.atomid{1};
        pocket_atom_sorted=cell2mat(pocket_atom_sorted);
        pocket_atom_sorted=sort(pocket_atom_sorted);
   else
        pocket_atom_sorted=pocket.atomid{1}; 
   end
   
   for atomLoop=1:1:no_atoms
       resno=resnos{atomLoop};
       
       if resno>0 && resno <= size(binary_amino,2)
           % Is this atom pocket-lining?
           % Note only need to count once - if flagged as yes no need to re-test
           if (binary_amino(resno) ==1) && (binary_atom(atomLoop)==1)
               % Already flagged...
           else
               
               if direct==1
                   % Direct-mode - pocket prog directly outputs pocket
                   % atoms/residues
                   atom=atnos(atomLoop);
                   pocket_lining = fn_pocketLiningDirect(params, pocket_atom_sorted, atom{1});
                   
               else
                   % Calculate pocket-lining based on nearest atoms/res to
                   % pocket points
                    pdb_xyz=xyz(atomLoop,:);
                    xyz_vector=vertcat(pdb_xyz, pocket_xyz);

                    new_vector=fn_applyLimits(params, xyz_vector);
                    pocket_lining=fn_pocketLining(params, new_vector);

               end
               
               % Pocket-lining counts...
               if pocket_lining==1
                    binary_amino(resno)=1;
                    binary_atom(atomLoop)=1;
                    restally(resno)=restally(resno)+1;
               end
               
           end
       else 
           % Some odd resnos in PDB i.e. few start '500' then restart 1...
           disp(strcat('Odd residue number for pdb_idx ', num2str(pdb_idx), ...
                    ' resno is:', num2str(resno), ' but binary_amino size is', num2str(size(binary_amino,2)),' - skipped'));
       end
   end
   
   % Now we need the average of atoms flagged as pocket lining grouped up
   % to residue level
   resnosmat=cell2mat(resnos);
   for iResLoop=1:1:maxres
       resavg(iResLoop)=restally(iResLoop)/(size(resnosmat(resnosmat==iResLoop),1));
       
   end
end

function [new_vector]=fn_applyLimits(params, xyz_vector)
    % Limits vector search space to be within a cube of 'pocketradius'
    % Much faster than searching all distant pocket points every time!
    
    %rA=params.pocketradius;
    rA=(params.pocketradius)*1.01;
    v_low=(xyz_vector(:,1)>(xyz_vector(1,1)-rA)) & (xyz_vector(:,2)>(xyz_vector(1,2)-rA)) & (xyz_vector(:,3)>(xyz_vector(1,3)-rA));
    v_high=(xyz_vector(:,1)<(xyz_vector(1,1)+rA)) & (xyz_vector(:,2)<(xyz_vector(1,2)+rA)) & (xyz_vector(:,3)<(xyz_vector(1,3)+rA));
    v_new=v_low & v_high;
    new_vector=xyz_vector(v_new,:);    
end

function [in_pocket] = fn_pocketLining(params,xyz_vector)
   in_pocket=0;
   
   % Calculate distances between points in vector
   dist_vector=pdist(xyz_vector);
   
   % Saves space by not using 'squareform' so is row vector
   % Only need distances from 1st col (the protein atom) to every other
   % pocket point not the distances between all pocket points.
   dist_vector_atom=dist_vector(1:size(xyz_vector,1)-1);
   dist_vector_logical=dist_vector_atom(dist_vector_atom< params.pocketradius);
   
   if ~isempty(dist_vector_logical) 
       
       % Check is NOT just a H atom
      % atomtype=pdb.atomtype2{pdb_idx}
      % if  
        in_pocket=1; 
      % end
   end
end

function [in_pocket]= fn_pocketLiningDirect(params, pocket, atom)
    in_pocket=0;
    for pocketLoop = 1:1:size(pocket,1)
        if pocket(pocketLoop)==atom
            in_pocket=1;
            break;
        end
        
    end
end

