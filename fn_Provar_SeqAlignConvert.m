%% RAPIDO format pairwise sequence alignment

function [aminoref_relative, aminoref_absolute, aminoref_absolute_inv,align_status]=fn_SeqAlignConvert(align_file)

 % align(<ref amino no>) = <comp struct amino no>)
 
 % Note use of relative/abs amino nos - see example below.

%   1A6UL             1A6WH                1A6UL                   1A6WH        Len.
%     1 -    25 <=>     2 -    26     ALA    2 - THR   26 <=> VAL  302 - GLY  326   25
%    26 -    26 <=>    27 -    27     GLY   27 - GLY   27 <=> TYR  327 - TYR  327    1
%    33 -    49 <=>    33 -    49     TYR   34 - ILE   50 <=> TRP  333 - GLY  349   17
%    50 -    52 <=>    50 -    52     GLY   51 - THR   53 <=> ARG  350 - ASP  352    3
%    53 -    59 <=>    58 -    64     ASN   54 - VAL   60 <=> THR  358 - PHE  364    7
%    60 -    60 <=>    65 -    65     PRO   61 - PRO   61 <=> LYS  365 - LYS  365    1
%    61 -    67 <=>    66 -    72     ALA   62 - LEU   68 <=> SER  366 - VAL  372    7
%    68 -    68 <=>    73 -    73     ILE   69 - ILE   69 <=> ASP  373 - ASP  373    1
%    69 -    70 <=>    76 -    77     GLY   70 - ASN   71 <=> SER  376 - SER  377    2
%    71 -    91 <=>    78 -    98     LYS   72 - LEU   92 <=> THR  378 - ARG  398   21
%    92 -    95 <=>    99 -   102     TRP   93 - ASN   96 <=> TYR  399 - TYR  402    4
%    97 -    97 <=>   108 -   108     TRP   98 - TRP   98 <=> ASP  408 - ASP  408    1
%    98 -   108 <=>   109 -   119     VAL   99 - LEU  109 <=> TYR  409 - SER  419   11
%                                                                                 ----
%                                                                                  101
 
% Here the relative/abs numbers don't matter as 1st starts at 1 (treat as exception!) 
%     1QIFA             1ACJA                1QIFA                   1ACJA        Len.
%     1 -   482 <=>     1 -   482     SER    4 - PRO  485 <=> SER    4 - PRO  485  482
%   487 -   532 <=>   483 -   528     SER  490 - THR  535 <=> SER  490 - THR  535   46
%                                                                                 ----
%                                                                                  528

    align_status='';
    
    fid=fopen(align_file);

    dataread=textscan(fid,'%u %c %u %s %u %c %u %*[^\n]','headerlines',1);
    %);
   
    % **** SIMPLE INITIAL READ WITH RELATIVE NUMBERS
    % Example data read (as cell arrays)
    %   1    2     3        4          5     6    7
    % [1;487] -- [482;532] <2x1 cell> [1;483] -- [482;528]
    
    %  ref st    ref ends              holo st     holo ends
    %  1        482                     1       482
    %  487      532                     483     528
    try
        if ~isempty(dataread)

            % How many translation rows are there?
            norows=size(dataread{1,1},1);

            % extract the highest amino number in the ref file
            highref=dataread{3}(norows);

            % Set up ref array to to no of aminos in ref file
            %  [1 2 3 4 ..... n]
            aminoref_relative=[1:highref];

            % Now go through each row of alignements and set up mapping
            % aminoref_relative(<ref amino>) = <aligned amino>

            for iLoop=1:1:norows
                refstart=dataread{1}(iLoop);
                refend=dataread{3}(iLoop);

                matchstart=dataread{5}(iLoop);
                matchend=dataread{7}(iLoop);

                aminoref_relative(refstart:refend)=[matchstart:matchend];

                % Set mapping of non-matched aminos to 0
                if iLoop>1
                    refend_prev=dataread{3}(iLoop-1);
                    aminoref_relative((refend_prev+1):(refstart-1))=zeros(1,refstart-refend_prev-1);  
                end
            end
        end
   
    catch
        err=lasterror;
        align_status=strcat('Error in align/simple:',err.message);
        aminoref_absolute(1)=0;
        aminoref_absolute_inv(1)=0;
    end
    
    fclose(fid);
    
        % *** FULL READ WITH ABSOLUTE NUMBERS
        % dataread_full column indices...
    %   1  2    3         5       7            9         12           15         18   19 	
        
        % ... corresp with number cols below
     %  1A6UL             1A6WH                1A6UL                   1A6WH        Len.
   %    1 -    25 <=>     2 -    26     ALA    2 - THR   26 <=> VAL  302 - GLY  326   25
   %   26 -    26 <=>    27 -    27     GLY   27 - GLY   27 <=> TYR  327 - TYR  327    1
    
    fid=fopen(align_file);
    
    try
        %                        1 -    25 <=>     2 -    26     ALA    2 - THR   26 <=> VAL  302 - GLY  326   25
        dataread_full=textscan(fid, ' %u %c  %u  %s    %u %c   %u      %s   %u %c %s   %u  %s  %s   %u %c %s   %u  %u','headerlines',1 );

       if ~isempty(dataread_full)

           % How many translation rows are there?
            norows=size(dataread_full{1,1},1);

            % extract the highest amino number in the ref file
            highref=dataread_full{12}(norows);
            % ... and lowest - no matches below this!
            lowref=dataread_full{9}(1)-1;

            % Set up ref array to to no of aminos in ref file
            %  [1 2 3 4 ..... n]
            aminoref_absolute=[1:highref];
             % Now go through each row of alignements and set up mapping
            % aminoref_absolute(<ref amino>) = <aligned amino>

            if lowref>1
                aminoref_absolute(1:lowref)=0;
            end

            for iLoop=1:1:norows
                refstart=dataread_full{9}(iLoop);
                refend=dataread_full{12}(iLoop);

                matchstart=dataread_full{15}(iLoop);
                matchend=dataread_full{18}(iLoop);

                if refstart<1, refstart=1; end;
                if refend<1, refend=1; end;
                if matchstart<1, matchstart=1; end;
                if matchend<1, matchend=1; end;

                % Need to account for gaps in numbering - e.g for 
                % 2OPP_A_1EET_A_matchlist.txt last line is:
                % HIS  539 - ASN  545 <=> HIS  539 - LYS  540    2
                matchend=matchend + ( (refend-refstart) - ( matchend-matchstart));

                aminoref_absolute(refstart:refend)=[matchstart:matchend];

                % Set mapping of non-matched aminos to 0
                if iLoop>1
                    refend_prev=dataread_full{12}(iLoop-1);
                    aminoref_absolute((refend_prev+1):(refstart-1))=zeros(1,refstart-refend_prev-1);  
                end
            end

            % Inverse mapping (is there a quicker way than loop?)
            aminoref_absolute_inv(max(aminoref_absolute))=0;
            for iLoop=1:1:size(aminoref_absolute,2)
                if aminoref_absolute(iLoop)>0
                    aminoref_absolute_inv(aminoref_absolute(iLoop))=iLoop;
                end
            end

       end
   
   catch
        err=lasterror;
        align_status=strcat('Error in align/absolute:',err.message);
        aminoref_absolute(1)=0;
        aminoref_absolute_inv(1)=0;
   end
    
   fclose(fid);
    
end