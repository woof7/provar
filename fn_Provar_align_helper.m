%% For sequence alignment index conversions between ClustalW alignments and
%  the PDB residue numbers
% 20/05/2011
% Created a params.mustang_align split as want to sep Clustal and Mustang
% processing...
% Error flagging
% 0 : Okay
% 1 : >1 chain in struct
% 2 : Can't find PDB in FASTA
% 4 : 

function [clustalref_to_pdbres, pdbres_to_clustalref, params]=fn_Provar_align_helper(params,pdb_struct)
    
    params.status.fn_Provar_align_helper='OK';
    
    try
    
        if params.mustang_align==1
            % Use Mustang version!
           [clustalref_to_pdbres, pdbres_to_clustalref]=fn_Provar_align_helper_mustang(params,pdb_struct);
           return;

        end

        clustalref_to_pdbres='';
        pdbres_to_clustalref='';

        if size(pdb_struct.chainIDs,1)>1
            params.status.fn_Provar_align_helper='Error: fn_Provar_align_helper: more than one chain passed into helper...';
            return
        end

        chainid=pdb_struct.chainIDs{1};
        pdbcode=pdb_struct.code;

        % Get this PDB's sequence alignment from file
        % FASTA or modified format?
        if params.useFASTA==1
            seq_align= fn_Provar_readFASTA(params,params.seq_align_file);

        else
            fid=fopen(params.seq_align_file);
            seq_align=textscan(fid,'%s %s','delimiter',',');
        end

        % How many aligments?
        no_aligns=size(seq_align{1},1);
        iAlignRef=0;
        for iAlignLoop=1:1:no_aligns
            thispdb=seq_align{1}{iAlignLoop};
            if strcmp(thispdb,strcat(pdbcode,chainid))
                iAlignRef=iAlignLoop;
                break;
            end
        end

        if iAlignRef==0
            params.status.fn_Provar_align_helper='Error: fn_Provar_align_helper: cannot find pdb in Clustal file...';
            return
        end

        aligned_seq=seq_align{2}{iAlignRef};

        % This translates a clustal position to residue pos...
        clustalref_to_pdbres=zeros(size(aligned_seq,2),1);
        % This translates a residue pos to a clustal pos...
        pdbres_to_clustalref=zeros(pdb_struct.chainarray{1} + pdb_struct.chainstart{1} - 1,1);

        % Loop PDB file
        prevresno=0;
        % 08/05/11 - this is causing spurious 1st letter assignments...
        % 01/06/11 - stick to old code when using Java solved res only
        % alignments...
        seq_start_idx=1;
        %seq_start_idx=pdb_struct.chainstart{1};   % The PDB start pos
        %non_gap_start=regexp(aligned_seq,'[^-]', 'once' );  % 1st non-gap in alignment
        %seq_start_idx=seq_start_idx + non_gap_start -1;

        %
        match_idx=1;
        no_atoms=size(pdb_struct.xyz{1},1);
        for iAtLoop=1:1:no_atoms
            thisresno=pdb_struct.resno{1}{iAtLoop};
            if thisresno==prevresno
                % ignore

            else
                % Get amino code for this residue
                amino_code=fn_Provar_getAminoLetter(pdb_struct.residue{1}{iAtLoop});

                % 09/05/11 - Adding one each time causes spurious matches
                % (as per start) if there are breaks in PDB seq... Need to
                % add on differentce in this/prev resnos (except at start
                % which is dealt with above) to skip matching
                % aminos in alignment but not in the PDB file.
                % 01/06/11 - stick to old code when using Java solved res only
                % alignments...

                if strcmp(amino_code,'?')
                    disp('*********');
                    disp(strcat(' *** NO CODE FOR ',pdb_struct.residue{1}{iAtLoop}));
                    disp('*********');
                end

                if prevresno == 0
                    actual_idx=seq_start_idx;
                else
                    %seq_start_idx=actual_idx + (thisresno - prevresno);
                    seq_start_idx=actual_idx + 1;

                end

                % Find match in Clustalseq
                align_subseq=aligned_seq(seq_start_idx:end);

                match_idx=regexp(align_subseq,amino_code,'once');


                if ~isempty(match_idx)
                    % actual index of aa from start (not just from subsequence start)
                    actual_idx=match_idx + seq_start_idx-1;

                    % actual_idx=match_idx + seq_start_idx-1;
                    pdbres_to_clustalref(thisresno)=actual_idx;
                    clustalref_to_pdbres(actual_idx)=thisresno;

                    %seq_start_idx=actual_idx+1;  * See note above
                else
                    % If we couldn't find code for some reason make sure we can
                    % start again on next!
                    match_idx=actual_idx;
                end

                prevresno=thisresno;
            end

        end
    catch
        err=lasterror;
        params.status.fn_Provar_align_helper=strcat('Error: fn_Provar_align_helper: ',err.message);
    end

end

