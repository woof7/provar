%% Run on a set of homologous structures
%  This differs from the CC/tCC code in that:
%  1) Files are simply indexed from 1 to however many proteins in dir and
%  matching PASS/LIGSITE files found on name (not conf number)
%  Proteins names should start with the PBDID (and optional chain ID)
%  2) probabilities corrected by using a ClustalW alignment to see how many
%  matched per residue.

% Clustal file - example in this directory as
% "clustal_from_chimera_reformat.csv"
% i.e. needs manual processing to flatten out to PDBID SEQ on a single line

function params = fn_Provar_homologues(params)
    params.status.fn_Provar_homologues='OK';
    try
        results='';
        %no_structs=0;
        results='';
        iFileSeq=0;
        iHomolSeq=0;

        % Set up position translation matrices of the reference sequence using
        % ClustalW alignment file
        [pdb_ref,conformer_no,params] = fn_Provar_loadStructure(params,params.ref_pdb);
        %pdb_ref= fn_Provar_loadStructure(params,status, params.ref_pdb);
        if strcmp(params.status.fn_Provar_loadStructure,'OK')
            params.status.homologues.ref.name=pdb_ref.code;
        else
            params.status.fn_Provar_homologues='Error:  fn_Provar_homologues: unable to load reference structure...';
            return;
        end

        [clustalref_to_pdbref, pdbref_to_clustalref,params]=fn_Provar_align_helper(params,pdb_ref);
        if strcmp(params.status.fn_Provar_align_helper,'OK')
            params.status.homologues.ref.fasta=0;
            
            if (sum(clustalref_to_pdbref)==0) || (sum(pdbref_to_clustalref) ==0)
                params.status.homologues.ref.fasta=1;
                params.status.fn_Provar_homologues=strcat('Error:  fn_Provar_homologues: fn_Provar_align_helper returned invalid alignment for *reference* structure (,', ...
                                                            params.ref_pdb, ') using supplied fasta file.');
                return;                 
             end
        end  

        % ** PROCESS STRUCTURE-PREDICTION PAIRS
        structure_files=dir(params.structure_dir);

        for iStructFileLoop=1:1:size(structure_files,1)
            if ~isempty(regexp(structure_files(iStructFileLoop).name,'.pdb', 'once'))
                try
                    % Load structure
                    struct_file_full=fullfile(params.structure_dir,structure_files(iStructFileLoop).name);
                    [structure,conf_no,params]=fn_Provar_loadStructure(params,struct_file_full);
                    if strcmp(params.status.fn_Provar_loadStructure,'OK')    
                        
                        % Create an index to use - we don't have conformation numbers!
                        iFileSeq=iFileSeq+1;
                        %params.status.homologues.loaded_structs=iFileSeq;
                    
                        % Status update...
                        params.status.homologues(iFileSeq).struct=structure_files(iStructFileLoop).name;
                        
                        % Get Clustal alignment translation...
                        [clustalhomol_to_pdbhomol, pdbhomol_to_clustalhomol,params]=fn_Provar_align_helper(params, structure);
                        if strcmp(params.status.fn_Provar_align_helper,'OK')
                             params.status.homologues(iFileSeq).fasta=0;
                            if (sum(clustalhomol_to_pdbhomol)==0) || (sum(pdbhomol_to_clustalhomol) ==0)
                                params.status.homologues(iFileSeq).fasta=1;
                                params.status.fn_Provar_homologues=strcat('Error:  fn_Provar_homologues: fn_Provar_align_helper returned invalid alignments with supplied fasta file...');
                                return;
                            end
                            
                        else
                            params.status.homologues(iFileSeq).fasta=1;
                            %disp(params.status.fn_Provar_align_helper);
                            params.status.fn_Provar_homologues=strcat('Error:  fn_Provar_homologues: fn_Provar_align_helper returned fail with supplied fasta file...');
                            return;
                        end


                        % Get the PDB code (and maybe chainID) e.g. 1M48B
                        pdbendpos=regexp(structure_files(iStructFileLoop).name,'.pdb', 'once');
                        pdbcode=structure_files(iStructFileLoop).name;
                        pdbcode=pdbcode(1:pdbendpos-1);

                        % Load pocket prediction (for all defined progs as ref'd in
                        % setParams).  Get struct array back in pocket_single - one for
                        % PASS, LIGSITE etc...
                       [pocket_single,params]=fn_Provar_loadPocketPredictionSingle(params,struct_file_full,pdbcode);

                         if strcmp(params.status.fn_Provar_loadPocketPredictionSingle,'OK')
                           

                            for iProgIdx=1:1:params.pocketProgNo
                                % Individual homologues
                                binary_amino{iProgIdx,iFileSeq}=0;
                                binary_atom{iProgIdx,iFileSeq}=0;
                                res_tally{iProgIdx,iFileSeq}=0;
                                % The aligned totals...
                                provar_amino_aligned{iProgIdx,iFileSeq}(pdb_ref.chainarray{1}+pdb_ref.chainstart{1}-1)=0;
                                res_avg_aligned{iProgIdx,iFileSeq}(pdb_ref.chainarray{1}+pdb_ref.chainstart{1}-1)=0;

                                % v4.7 these globals aligned to ref at very end...
                                provar_global_amino_to_ref{iProgIdx,iFileSeq}(pdb_ref.chainarray{1}+pdb_ref.chainstart{1}-1)=0;
                                provar_global_avg_to_ref{iProgIdx,iFileSeq}(pdb_ref.chainarray{1}+pdb_ref.chainstart{1}-1)=0;

                                % * This refers to position in the clustalw file only
                                % irrespective of the reference structure *
                                provar_global_amino{iProgIdx}(size(clustalhomol_to_pdbhomol,1))=0;
                                provar_global_avg{iProgIdx}(size(clustalhomol_to_pdbhomol,1))=0;
                                %provar_global_var_array{iProgIdx}=zeros(size(structure_files,1),size(clustalhomol_to_pdbhomol,1));  %v4.7

                                % Only if we have pocket data for this prog!
                                %if ~isempty(pocket_single{iProgIdx})
                                if pocket_single{iProgIdx}.status==0 
                                
                                    params.status.homologues(iFileSeq).pocketprogs(iProgIdx)=0;

                                    % Get pocket lining residues and atoms...
                                    [binary_amino{iProgIdx,iFileSeq}, binary_atom{iProgIdx,iFileSeq}, res_tally{iProgIdx,iFileSeq},res_avg{iProgIdx,iFileSeq}] =  ...
                                        fn_getPocketLining(params, structure, 1,pocket_single{iProgIdx},params.pocketDirect{iProgIdx} );

                                    % Align the homologous sequence back to the
                                    % reference seq (only works for residues!)
                                    for iHomolpdb_idx=1:1:size(binary_amino{iProgIdx,iFileSeq},2)

                                        % Convert PDBh to clustal aligned posn
                                        iHomolClustal_idx=pdbhomol_to_clustalhomol(iHomolpdb_idx);

                                        % Convert this clustal pos to the reference structure's
                                        % PDB idx
                                        if iHomolClustal_idx==0
                                            % Leave totals as '-1' = not aligned
                                        else

                                            % Clustal totals (these are not related to
                                            % reference PDB!)
                                            provar_global_amino{iProgIdx}(iHomolClustal_idx)= ...
                                                provar_global_amino{iProgIdx}(iHomolClustal_idx) + binary_amino{iProgIdx,iFileSeq}(iHomolpdb_idx);

                                            provar_global_avg{iProgIdx}(iHomolClustal_idx)= ...
                                                provar_global_avg{iProgIdx}(iHomolClustal_idx) + res_avg{iProgIdx,iFileSeq}(iHomolpdb_idx);

                                            iRefpdb_idx=clustalref_to_pdbref(iHomolClustal_idx);

                                            if iRefpdb_idx==0

                                                % Leave as '-1'=not aligned
                                            else
                                                % Save totals in the *aligned* position
                                                provar_amino_aligned{iProgIdx,iFileSeq}(iRefpdb_idx)=binary_amino{iProgIdx,iFileSeq}(iHomolpdb_idx);
                                                res_avg_aligned{iProgIdx,iFileSeq}(iRefpdb_idx)=res_avg{iProgIdx,iFileSeq}(iHomolpdb_idx);
                                            end
                                        end
                                    end
                                    
                                else
                                    % No data for this program...
                                    params.status.homologues(iFileSeq).pocketprogs(iProgIdx)=-1;
                                  %  disp(strcmp('Warning: Failed to load pocket prediction for :',params.pocketProg{iProgIdx}));
                                end
                            end     % iProgIdx
                           
                            
                        else
                                                        disp(params.status.fn_Provar_loadPocketPredictionSingle);
                        end         % if pocketpredsingle OK

                        
                    else
                        
                    end     % if /load structure
        
                catch
                    err=lasterror;    
                    params.status.fn_Provar_homologues=strcat('Error:  fn_Provar_homologues: filename: ',structure_files(iStructFileLoop).name,':',err.message);
                    disp(params.status.fn_Provar_homologues);
                    
                end   % try / catch
                
            end   % if/~isempty struct files
        end  % Structure loop

        % This gives us number of aligned residues for each position in the
        % FASTA file (so we still need to align these later too..!)
        [missingScores]=fn_Provar_missingScore(params);
        
        % Total up all aligned Provar binaries...
        for iProgIdx=1:1:params.pocketProgNo
            for iHom_idx=1:1:size(provar_amino_aligned,2)
                % Create totals in context of homologous structure -
                % *not yet aligned to reference *
                if iHom_idx==1
                    provar_amino_aligned_total{iProgIdx}=provar_amino_aligned{iProgIdx,iHom_idx};
                    %provar_atom_total{iProgIdx}= binary_atom{iProgIdx,iHom_idx};
                    provar_amino_avg_aligned_total{iProgIdx}=res_avg_aligned{iProgIdx,iHom_idx};
                else

                    [provar_amino_aligned_total{iProgIdx},binary_amino_aligned{iProgIdx,iHom_idx}]=fn_Provar_padarray(provar_amino_aligned_total{iProgIdx},provar_amino_aligned{iProgIdx,iHom_idx});
                    provar_amino_aligned_total{iProgIdx}=provar_amino_aligned_total{iProgIdx} + provar_amino_aligned{iProgIdx,iHom_idx};

                    %[provar_atom_total{iProgIdx},binary_atom{iProgIdx,iHom_idx}]=fn_Provar_padarray(provar_atom_total{iProgIdx}, binary_atom{iProgIdx,iHom_idx});
                    %provar_atom_total{iProgIdx}= provar_atom_total{iProgIdx} + binary_atom{iProgIdx,iHomolSeq};

                    [provar_amino_avg_aligned_total{iProgIdx},res_avg_aligned{iProgIdx,iHom_idx}]=fn_Provar_padarray(provar_amino_avg_aligned_total{iProgIdx},res_avg_aligned{iProgIdx,iHom_idx});
                    provar_amino_avg_aligned_total{iProgIdx}=provar_amino_avg_aligned_total{iProgIdx} + res_avg_aligned{iProgIdx,iHom_idx};

                end

            end

            % Gaps in alignments need to be flagged up as '-1'
            provar_amino_aligned_total{iProgIdx}(pdbref_to_clustalref==0)=-1;
            provar_amino_avg_aligned_total{iProgIdx}(pdbref_to_clustalref==0)=-1;

            % ***************
            % These are the probabilities used for rendering - i.e. the
            % probabilities for each residue of being pocket lining, across
            % the aligned superfamily, mapped onto the reference struct...
            % Loop all residues in reference structure
            for iResIdx=1:1:(pdb_ref.chainarray{1} + pdb_ref.chainstart{1}-1)
                if provar_amino_aligned_total{iProgIdx}(iResIdx) >-1
                    provar_amino_aligned_total{iProgIdx}(iResIdx)=provar_amino_aligned_total{iProgIdx}(iResIdx)/missingScores(pdbref_to_clustalref(iResIdx));
                end

                if provar_amino_avg_aligned_total{iProgIdx}(iResIdx) >-1
                    provar_amino_avg_aligned_total{iProgIdx}(iResIdx)=provar_amino_avg_aligned_total{iProgIdx}(iResIdx)/missingScores(pdbref_to_clustalref(iResIdx));
                end
            end
            % ***************
            
            % Update the clustal totals based on total aligments at each
            % position
            % These are probabilities for every global alignment position
            % in the FASTA file - naturally some of these won't exist in
            % the reference structure.  These are written to a probability
            % file only (there isn't a 'real' pdb to write them to!)
            for iResIdx=1:1:size(clustalhomol_to_pdbhomol,1)
                if provar_global_amino{iProgIdx}(iResIdx)>-1
                    provar_global_amino{iProgIdx}(iResIdx)=provar_global_amino{iProgIdx}(iResIdx)/missingScores(iResIdx);
                end
                if provar_global_amino{iProgIdx}(iResIdx)>-1
                    provar_global_avg{iProgIdx}(iResIdx)= provar_global_avg{iProgIdx}(iResIdx)/missingScores(iResIdx);
                end

%                 %  v4.7
%                 % Align these globals to reference structure
%                 iRefpdb_idx=clustalref_to_pdbref(iResIdx);
%                 if iRefpdb_idx==0
%                     % leave as -1
%                 else
%                     provar_global_amino_to_ref{iProgIdx}(iRefpdb_idx)=provar_global_amino{iProgIdx}(iResIdx);
%                     provar_global_avg_to_ref{iProgIdx}(iRefpdb_idx)=provar_global_avg{iProgIdx}(iResIdx);
%                   %  provar_global_var{iProgIdx}(iRefpdb_idx)=var(provar_global_var_array{iProgIdx}(:,iResIdx));
%                    % provar_global_stddev{iProgIdx}(iRefpdb_idx)=std(provar_global_var_array{iProgIdx}(:,iResIdx));
%                 end                              
            end    
            
 
            % ***************************************************
            % Update stats and generate probability and PDB files
            % ***************************************************
            if (params.progrun{iProgIdx}==1) && (pocket_single{iProgIdx}.status==0)

                 % Quantiles
                 % *********
                 % * For homologues the quantiles are based on the
                 % distribution of the global FASTA alignment, NOT just the
                 % residues found in the (perhaps arbitrary) reference
                 % structure *
                 % *********
                 params.stats{iProgIdx}.amino=num2str(sprintf('% 1.3f',quantile(provar_global_amino{iProgIdx},[.25,0.50,0.75])));
                 params.stats{iProgIdx}.amino_avg=num2str(sprintf('% 1.3f',quantile(provar_global_avg{iProgIdx},[.25,0.50,0.75])));

                 disp('');
                 disp(strcat('Quantiles  (GLOBAL ALIGNMENT) ( 0.25,0.50,0.75) for :',params.pocketProg{iProgIdx}));
          
                 disp(strcat('amino (global)                : ',params.stats{iProgIdx}.amino));
                 disp(strcat('amino avg (global)            : ',params.stats{iProgIdx}.amino_avg));

                 % Update params with probabilities
                 % Global
                 params.p.provar_global_amino{iProgIdx}=provar_global_amino{iProgIdx};
                 params.p.provar_global_avg{iProgIdx}=provar_global_avg{iProgIdx};
                 
                 % Reference (these used for ref PDB update)
                 params.p.provar_amino_p{iProgIdx}=provar_amino_aligned_total{iProgIdx};
                 params.p.provar_amino_avg_p{iProgIdx}=provar_amino_avg_aligned_total{iProgIdx};

                 disp('----------------------------------------------------------');

                 % -------------------------------------
                 % Write output probability files ...
                 disp('Writing probability files....');
                 params= fn_Provar_write_pb_files(params,provar_amino_aligned_total,'p',iProgIdx,0);
                 params= fn_Provar_write_pb_files(params,provar_amino_avg_aligned_total,'p',iProgIdx,2);
                
                 params= fn_Provar_write_pb_files(params,provar_global_amino,'p',iProgIdx,3);
                 params= fn_Provar_write_pb_files(params,provar_global_avg,'p',iProgIdx,4);
                
                 disp(params.status.fn_Provar_write_pb_files);
                 disp('');

                 % Write PDB output files (update the B-factor with probability)
                 disp('Writing PDB files....');
                 params=fn_Provar_write_pdb_files(params, iProgIdx);
                 disp(params.status.fn_Provar_write_pdb_files);
                 disp('');

            end

% 
% 
%             % Bfactor form
%             provar_amino_aligned_total_b{iProgIdx}=100*provar_amino_aligned_total{iProgIdx};
%             provar_amino_aligned_total_b{iProgIdx}(provar_amino_aligned_total_b{iProgIdx}>99.99)=99.99;
% 
%             provar_amino_avg_aligned_total_b{iProgIdx}=100*provar_amino_avg_aligned_total{iProgIdx};
%             provar_amino_avg_aligned_total_b{iProgIdx}(provar_amino_avg_aligned_total_b{iProgIdx}>99.99)=99.99;

        end

    catch
        err=lasterror;
        params.status.fn_Provar_homologues=strcat('Error in fn_Provar_homologues:',err.message);

    end 
    
    
end
