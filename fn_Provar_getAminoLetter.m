%% Translate amino codes 3 letter to 1

function [aminoLetter]=fn_Provar_getAminoLetter(aminoCode)

    aminoCode=upper(aminoCode);

    switch aminoCode
        case 'ALA'
            aminoLetter='A';

        case 'ARG'
            aminoLetter='R';

        case 'ASN'
            aminoLetter='N';

        case 'ASP'
            aminoLetter='D';

        case 'CYS'
            aminoLetter='C';

        case 'GLU'
            aminoLetter='E';

        case 'GLN'
            aminoLetter='Q';

        case 'GLY'
            aminoLetter='G';

        case 'HIS'
            aminoLetter='H';

        case 'ILE'
            aminoLetter='I';

        case 'LEU'
            aminoLetter='L';

        case 'LYS'
            aminoLetter='K';

        case 'MET'
            aminoLetter='M';
            
        case 'MSE'
            aminoLetter='M';  % 01/06/2011

        case 'PHE'
            aminoLetter='F';

        case 'PRO'
            aminoLetter='P';

        case 'SER'
            aminoLetter='S';

        case 'THR'
            aminoLetter='T';

        case 'TRP'
            aminoLetter='W';

        case 'TYR'
            aminoLetter='Y';

        case 'VAL'
            aminoLetter='V';

        % Ambiguous

        case 'ASX'
            aminoLetter='B';

        case 'GLX'
            aminoLetter='Z';

        case 'XLE'
            aminoLetter='J';

        case 'XAA'
            aminoLetter='X';

        otherwise
            aminoLetter='?';
    end


end