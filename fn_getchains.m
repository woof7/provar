% Process PDB chains
function [chainids,chainarray,chainstart]=fn_getchains(chainset,resnoset)     
    
    if iscell(chainset)
        chainset=cell2mat(chainset);
    end
    
    if iscell(resnoset)
        resnoset=cell2mat(resnoset);
    end
    
    % What are unique chains?
    chains=unique(chainset);
    
    
    % How many chains?
    nochains=size(chains,2);
    
    % Initialise chain arrays
    chainarray(nochains)=0;
    chainstart(nochains)=0;
    
    % Loop each chain and get starting AA number and total number
    for iLoop=1:1:nochains
        
        % Find residue number range for each chain
        chainsetlogical=chainset==chains(iLoop);
        %resnos=cell2mat(resnoset(chainsetlogical));
        resnos=resnoset(chainsetlogical);
        
        chainstart(1,iLoop)=min(resnos);
        chainarray(1,iLoop)=max(resnos)-min(resnos)+1;
    
    end
    
    chainids=chains;
    
end