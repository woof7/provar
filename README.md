# README #

Please see documentation in doc/Provar_UserGuide.pdf

Ashford, P. et al., 2012. Visualisation of variable binding pockets on protein surfaces by probabilistic analysis of related structure sets. BMC bioinformatics, 13, p.39.

