% ****************************
% Provar initialisation
% ****************************

function params=fn_Provar_init()
% Prog-wide params to set before run - e.g. file directories
    
    % **************************************************
    % Global parameters
    % **************************************************
    params.voxsize=4;   %PDF3
    params.pocketradius=3.75;  %PDF4  
    params.provar_version='4.81';
    
    % How many pocket prediction progs are potentially available?
    % Specific types parameterised below.
    % Note can use any no beween 1 and params.pocketProgNo for any given
    % run.
    params.pocketProgNo=8;
    
    % Following blocks parameterise the pocket-prediction programs so the
    % modules know what file names to look out for depending on the type of
    % program.  Also simplify the name to start with two letter code e.g.
    % PA=PASS which can then start folder names.
    params.pocketProg{1}='PASS';
    params.pocketProgFileSuffix{1}='_probes.pdb';
    params.pocketProgAbbrev{1}='PA';
    params.pocketDirect{1}=0;  % Direct pocket lining output from prog?
    params.progrun{1}=0;    % default to not run
    params.progid{1}='';    % default to not run
    params.pass_dir='';   % user-friendly name for this set
    
    params.pocketProg{2}='LIGSITE';
    params.pocketProgFileSuffix{2}='.pdb_pocket_r.pdb';
    params.pocketProgAbbrev{2}='LC';
    params.pocketDirect{2}=0;  % Direct pocket lining output from prog?
    params.progrun{2}=0;    % default to not run
    params.progid{2}='';    % default to not run
    params.ligsite_dir='';   % user-friendly name for this set
    
    params.pocketProg{3}='SITEMAP';
    params.pocketProgFileSuffix{3}='_sitemap.pdb';
    params.pocketProgAbbrev{3}='SM';
    params.pocketDirect{3}=0;  % Direct pocket lining output from prog?
    params.progrun{3}=0;    % default to not run
    params.progid{3}='';    % default to not run
    params.sitemap_dir='';   % user-friendly name for this set
    
    % Uses the fPocket verteces as pocket points similar to 
    % PASS/LIGSITE
    % Get the predictions out of the subfolders first...
    % e.g. in directory:    /Users/paul/bioinf/run/TC00053/removed_H
    %      cp -R tdisco*_out/tdisco*_pockets.pqr /Users/paul/bioinf/run/FV00010
    params.pocketProg{4}='fPocket_vertices';
    params.pocketProgFileSuffix{4}='_pockets.pqr';
    params.pocketProgAbbrev{4}='FV';
    params.pocketDirect{4}=0;  % Direct pocket lining output from prog?
    params.progrun{4}=0;    % default to not run
    params.progid{4}='';    % default to not run
    params.fpocket_vert_dir='';   % user-friendly name for this set
    
    % Uses the direct output of pocket lining atoms/residues 
    params.pocketProg{5}='fPocket_direct';
    params.pocketProgFileSuffix{5}=fullfile('_out','/pockets/');
    params.pocketProgAbbrev{5}='FD';
    params.pocketDirect{5}=1;  % Direct pocket lining output from prog?
    params.progrun{5}=0;    % default to not run
    params.progid{5}='';    % default to not run
    params.fpocket_dir='';   % user-friendly name for this set
    
    % Uses fPocket ATOM records in standard way
    params.pocketProg{6}='fPocket_atom';
    params.pocketProgFileSuffix{6}=fullfile('_out','/pockets/');
    params.pocketProgAbbrev{6}='FA';
    params.pocketDirect{6}=0;  % Direct pocket lining output from prog?
    params.progrun{6}=0;    % default to not run
    params.progid{6}='';    %
    params.fpocket_atom_dir='';
    
    % Uses fPocket ATOM records in standard way
    params.pocketProg{6}='fPocket_atom';
    params.pocketProgFileSuffix{6}=fullfile('_out','/pockets/');
    params.pocketProgAbbrev{6}='FA';
    params.pocketDirect{6}=0;  % Direct pocket lining output from prog?
    params.progrun{6}=0;    % default to not run
    params.progid{6}='';    %
    params.fpocket_atom_dir='';
    
    % Uses any generic PDB file with pocket points
    params.pocketProg{7}='Generic_pocket_PDB';
    params.pocketProgFileSuffix{7}='_pocket.pdb';
    params.pocketProgAbbrev{7}='GP';
    params.pocketDirect{7}=0;  % Direct pocket lining output from prog?
    params.progrun{7}=0;    % default to not run
    params.progid{7}='';    %
    params.generic_pocket_dir='';
    
    % Uses any generic PDB file with pocket-lining atoms (e.g. CASTp)
    % "Direct mode"
    params.pocketProg{8}='Generic_pocket_direct_PDB';
    params.pocketProgFileSuffix{8}='_pocket.pdb';
    params.pocketProgAbbrev{8}='GD';
    params.pocketDirect{8}=1;  % Direct pocket lining output from prog?
    params.progrun{8}=0;    % default to not run
    params.progid{8}='';    %
    params.generic_pocket_direct_dir='';
    
    % Other defaults
    params.homolSkip='';
    params.homSkip{1}=('');  
    params.mustang_align=0;
   	params.useFASTA=0;
    params.seq_align_file='';
    params.multirun=0;
       
end